# Copyright Map

## Code

All MyDot source code is licensed under the GNU GPL 2.0 "or-later" license.

License: [GPL-2.0-or-later](./GPL-2.0-or-later.txt)

## Assets

The MyDot logo is licensed under the CC-BY-4.0 license.

License: [CC-BY-4.0](./CC-BY-4.0.txt)
