#!/bin/sh


# This file is part of mydot.

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# shellcheck disable=1090


nullwrap() {
    "${@}" >/dev/null 2>&1
}

command_exists() {
    nullwrap command -v "${1}"
}

source_file() {
    [ -f "${1}" ] && . "${1}"
}


# Run system's xinitrc.d scripts
if [ -d /etc/X11/xinit/xinitrc.d ] ; then
    for f in /etc/X11/xinit/xinitrc.d/?* ; do
        source_file "${f}"
    done
    unset f
fi

# Polish keys + holding Caps Lock acts as Control
if command_exists setxkbmap ; then
    setxkbmap pl -option "caps:ctrl_modifier" &
fi

# Use Caps Lock as Escape
if command_exists xcape ; then
    xcape -e "Caps_Lock=Escape" &
fi

# Xset
if command_exists xset ; then
    # disable beeping
    xset b off
    # screensaver timeout
    xset s 1200 0
fi

# Xrdb
if [ -r "${HOME}/.Xresources" ] && command_exists xrdb ; then
    xrdb "${HOME}/.Xresources"
fi

# Basic X root
command_exists xsetroot && xsetroot -solid grey
