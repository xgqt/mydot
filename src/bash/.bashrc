#!/usr/bin/env bash

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# ~/.bashrc
# shellcheck disable=1090

# Kill Switch

if [[ $- != *i* ]] ; then
    # Shell is non-interactive. Be done now!
    return
fi

# Common settings

if [[ -e "${HOME}/.profile" ]] ; then
    source "${HOME}/.profile"
fi

# History

# File location
if [[ -n "${XDG_CACHE_HOME}" ]] ; then
    mkdir -p "${XDG_CACHE_HOME}/bash"

    export HISTFILE="${XDG_CACHE_HOME}/bash/history"
else
    mkdir -p "${HOME}/.cache/bash"

    export HISTFILE="${HOME}/.cache/bash/history"
fi

# Size
HISTSIZE="90000"
HISTFILESIZE="90000"

# No double entries in the shell history
HISTCONTROL="${HISTCONTROL} erasedups:ignoreboth"

# Safety

# Set root's editor to vi
if am_i_root ; then
    if command_exists vi ; then
        EDITOR="vi"

        export EDITOR
    fi
fi

# Prompt theme

declare __dumb_PS1=$'\h -> '
declare __root_PS1=$'\[$(tput bold)\]\[$(tput setaf 1)\]\h \[$(tput setaf 3)\]\w \[$(tput sgr0)\]
\[$(tput bold)\]\[$(tput setaf 2)\]-> \[$(tput sgr0)\]'
declare __user_PS1=$'\[$(tput bold)\]\[$(tput setaf 4)\]\h \[$(tput setaf 3)\]\w \[$(tput sgr0)\]
\[$(tput bold)\]\[$(tput setaf 2)\]-> \[$(tput sgr0)\]'

case "${TERM}" in
    "" | dumb )
        PS1="${__dumb_PS1}"
        ;;
    * )
        if am_i_root ; then
            PS1="${__root_PS1}"
        else
            PS1="${__user_PS1}"
        fi
        ;;
esac

unset __dumb_PS1
unset __root_PS1
unset __user_PS1

# Bash tweaks

# Auto-change directory
shopt -s autocd

# Append history
shopt -s histappend
