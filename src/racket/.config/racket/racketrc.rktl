;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License

;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.


;;; ~/.racketrc / ~/.local/share/racket/racketrc.rktl

;; Some Racket REPL customizations

;; This file isn't loaded at startup
;; if we define the language, like:
;; #lang racket


(require (only-in setup/collection-search collection-search))


(for ([mod '(math/flonum      ; floats
             racket/bool      ; logic operations
             racket/contract  ; contracts
             racket/format    ; values to strings
             racket/function  ; negate, *join
             racket/port      ; port procedures
             racket/trace     ; tracing
             xrepl/xrepl      ; enhanced REPL
             ;; Non-main-distribution packages
             threading
             uuid)])
  (let* ([mod-str (symbol->string mod)]
         [mod-lib (string-append mod-str "/main.rkt")])
    (cond
      [(collection-search `(lib ,mod-lib))
       (eval `(require ,mod))]
      [else
       (printf "WARNING!: Collection ~v not found!~%" mod-str)])))


(define-syntax produce-hash
  (syntax-rules ()
    [(_ function v ...)
     (make-hash (list (cons v (function v)) ...))]))


(define (print-system-paths)
  (define system-path-hash
    (produce-hash find-system-path
                  'addon-dir
                  'collects-dir
                  'config-dir
                  'desk-dir
                  'doc-dir
                  'exec-file
                  'home-dir
                  'host-collects-dir
                  'host-config-dir
                  'init-dir
                  'init-file
                  'orig-dir
                  'pref-dir
                  'pref-file
                  'run-file
                  'sys-dir
                  'temp-dir))
  (for ([(key value) (in-hash system-path-hash)])
    (printf "; ~a = ~a~%" key value)))


(print-system-paths)

(display "\
\e[0m; \e[31m                 \e[34m.,;;;;;;;;;;;;;;,'.
\e[0m; \e[31m                   \e[34m.,;;;;;;;;;;;;;;;;.
\e[0m; \e[31m       ..            \e[34m.';;;;;;;;;;;;;;;;,.
\e[0m; \e[31m     .'''''..          \e[34m.';;;;;;;;;;;;;;;;,.
\e[0m; \e[31m   .''''''''''.           \e[34m';;;;;;;;;;;;;;;;.
\e[0m; \e[31m  .'''''''''''''..          \e[34m';;;;;;;;;;;;;;;;.
\e[0m; \e[31m .'''''''''''''''''.         \e[34m.,;;;;;;;;;;;;;;;.
\e[0m; \e[31m.''''''''''''''''''.           \e[34m';;;;;;;;;;;;;;,
\e[0m; \e[31m'''''''''''''''''.              \e[34m.;;;;;;;;;;;;;;'
\e[0m; \e[31m'''''''''''''''..                 \e[34m';;;;;;;;;;;;;
\e[0m; \e[31m''''''''''''''.        .''         \e[34m.;;;;;;;;;;;;
\e[0m; \e[31m'''''''''''''.       .'''''.        \e[34m.;;;;;;;;;;;
\e[0m; \e[31m'''''''''''.        .'''''''.        \e[34m.;;;;;;;;;;
\e[0m; \e[31m''''''''''.        .'''''''''.        \e[34m.;;;;;;;;;
\e[0m; \e[31m'''''''''.        ''''''''''''.        \e[34m.;;;;;;;.
\e[0m; \e[31m '''''''.        ''''''''''''''.        \e[34m.;;;;;,
\e[0m; \e[31m `'''''.        ''''''''''''''''.        \e[34m.;;;,
\e[0m; \e[31m  `''''        ''''''''''''''''''.        \e[34m,;,
\e[0m; \e[31m    `'        .'''''''''''''''''''        \e[34m..
\e[0m; \e[31m             .''''''''''''''''''''.
\e[0m; \e[31m             ''''''''''''''''''''''
\e[0m; \e[31m            .''''''''''''''''''''''
\e[0m; \e[31m             ```'''''''''''''''''
\e[0m")
