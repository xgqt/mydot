#!/usr/bin/env python


"""
IPython configuration.
""" """
This file is part of mydot - XGQT's configuration files.
Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License

mydot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

mydot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mydot.  If not, see <https://www.gnu.org/licenses/>.
"""


try:
    ipython_config = get_config()
except NameError as e:
    print("Could not set ipython_config")
    print(f"Full error: {e}")


ipython_config.TerminalInteractiveShell.confirm_exit = False
