#!/usr/bin/env guile
!#


;; This file is part of mydot.

;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License


;;; ~/.guile

;; Some Guile REPL customizations.


;;; Environment

;; History file (setting it here doesn't work)
;; (setenv "GUILE_HISTORY" "$HOME/.cache/guile/history")


;;; Aliases

(define (first lst)
  (car lst))

(define (rest lst)
  (cdr lst))

(define (last lst)
  (car (reverse lst)))


;;; Helpers

(define (show . args)
  "Dispaly any arguments."
  (display (apply string-append args)))

(define (reload)
  "Load the Guile RC file"
  (load (string-append (getenv "HOME") "/.guile")))

(define (list->random lst)
  "Get a random element of a list."
  (list-ref lst (random (length lst))))

(define (atoms->random . args)
  "From among given atoms select one at random."
  (list->random args))

(define (file->chars file)
  "Read a file into a list of characters."
  (with-input-from-file file
    (lambda ()
      (let reading ((chars '()))
        (let ((char (read-char)))
          (cond
           ((eof-object? char)
            (reverse chars))
           (else
            (reading (cons char chars)))))))))

(define (display-file file)
  "Display a file."
  (display (list->string (file->chars file))))


;;; Modules

;; Readline interface
(cond
 ((false-if-exception (resolve-interface '(ice-9 readline)))
  =>
  (lambda (module)
    ((module-ref module 'activate-readline))))
 (else
  (show "Consider installing the \"guile-readline\" package." "\n")))

;; Colorized REPL
;; https://gitlab.com/NalaGinrut/guile-colorized
(cond
 ((false-if-exception (resolve-interface '(ice-9 colorized)))
  =>
  (lambda (module)
    ((module-ref module 'activate-colorized))))
 (else
  (show "Consider installing the \"guile-colorized\" package." "\n")))


;;; Greeter

(show
 "\n"
 "        .,;::::.    ......                                      " "\n"
 "     ..;:::::'`       .......                                   " "\n"
 "   .;::::::'`           .......                                 " "\n"
 "  .;::::;'    .,;:;,.     ......         ...  ...               " "\n"
 " .;::::;.  ;kNMMMWMMMKd.   ......       .WMN  xMM.              " "\n"
 " ;:::::  .KMMk;`  `'oNMWx   ;:;...       ```  xMM.     .;:;.    " "\n"
 ".:::::' .XMW:         ```  ,MMO... 'MMo .WMN  xMM.  .'WMNXNWWk' " "\n"
 "':::::. ;MM0   ..........  ,MMO... 'MMo .WMN  xMM. :WM0,.  .xMW;" "\n"
 "::::::. .WMN.  cXXXXXXNNNK ,MMO... 'MMo .WMN  xMM. KMMNXXXXXNMM0" "\n"
 "`:::::;  cWMK;      .oWMW; 'MMK... cMMo .WMN  xMM. xMMd     ...." "\n"
 "`::::::   'kWMNOxdx0WMWx.  .0MMKxdkWMMo .WMN  xMM.  dWMKdooOWMk." "\n"
 " `::::::.  `:okO00ko:'`   ...ck00Od;x;   xkd  ckk.   `:dO00kl`  " "\n"
 "  `::::::.               ......                                 " "\n"
 "   `::::::.            ......                                   " "\n"
 "     ``:::::.        .....                                      " "\n"
 "\n")
