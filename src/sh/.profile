#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# ~/.profile: executed by the command interpreter for LOGIN shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.
#
# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.

# Shellcheck
#
# Ignore "Can't follow non-constant source."
# We just don't carea bout that
# shellcheck disable=1090
# https://github.com/koalaman/shellcheck/wiki/SC1090
#
# Ignore "Expressions don't expand in single quotes,
#         use double quotes for that."
# We want that for aliases
# shellcheck disable=2016
# https://github.com/koalaman/shellcheck/wiki/SC2016
#
# Ignore "This expands when defined, not when used. Consider escaping."
# We want that for aliases
# shellcheck disable=2139
# https://github.com/koalaman/shellcheck/wiki/SC2139

# Helper functions

# Send stdout and stderr of 'command' to /dev/null
# Basically this will return the exit status
nullwrap() {
    "${@}" >/dev/null 2>&1
}

command_exists() {
    if [ -z "${1}" ] ; then
        echo "Usage: command_exists COMMAND"
        echo "command_exists - check if COMMAND exists"

        return 1
    else
        nullwrap type "${1}"
    fi
}

a_k_a() {
    if [ -z "${2}" ] ; then
        echo "Usage: a_k_a ALIAS COMMAND"
        echo "a_k_a - safe alias"
        echo "will not create ALIAS if COMMAND is already occupied"

        return 1
    else
        ! command_exists "${1}" && alias "${1}"="${2}"
    fi
}

# For bigger cases just use if statement, see python below.
rbind() {
    if [ -z "${3}" ] ; then
        echo "Usage: rbind PROGRAM ALIAS COMMAND [OPTION]"
        echo "rbind - alias COMMAND to ALIAS if PROGRAM exists"
        echo
        echo "Options:"
        echo "    -s, --safe    do not alias if ALIAS already exists"
        echo "    -u, --unsafe  alias regardless of ALIAS (default)"

        return 1
    else
        if command_exists "${1}" ; then
            case "${4}" in
                "" | -u | -unsafe | --unsafe )
                    alias "${2}"="${3}"
                    ;;
                -s | -safe | --safe )
                    a_k_a "${2}" "${3}"
                    ;;
            esac
        fi
    fi
}

contains_string() {
    if [ -z "${2}" ] ; then
        echo "Usage: contains_string STRING1 STRING2"
        echo "contains_string - check if STRING1 contains STRING2"

        return 1
    else
        [ "${1#*"${2}"}" != "${1}" ]
    fi
}

add_to_path() {
    if [ -z "${1}" ] ; then
        echo "Usage: add_to_path PATH"
        echo "add_to_path - append given PATH to your PATH"
        echo "this function first checks if specified directory"
        echo "already is in PATH, if it is not and it exists"
        echo "it is added to the PATH"

        return 1
    fi

    if ! [ -d "${1}" ] ; then
        return 0
    fi

    if contains_string "${PATH}" "${1}" ; then
        return 0
    fi

    export PATH="${PATH}:${1}"
}

add_to_manpath() {
    if [ -z "${1}" ] ; then
        echo "Usage: add_to_manpath PATH"
        echo "add_to_manpath - append given PATH to your MANPATH"
        echo "this function first checks if specified directory"
        echo "already is in MANPATH, if it is not and it exists"
        echo "it is added to the MANPATH"

        return 1
    fi

    if ! [ -d "${1}" ] ; then
        return 0
    fi

    if contains_string "${MANPATH}" "${1}" ; then
        return 0
    fi

    export MANPATH="${MANPATH}:${1}"
}

# Are you root?
am_i_root() {
    [ "$(whoami)" = "root" ]
}

mkpath() {
    if [ -z "${1}" ] ; then
        echo "Usage: mkpath PATH"
        echo "mkcd - silently make a directory including its parents"

        return 1
    else
        nullwrap mkdir -p "${*}" || return 1
    fi
}

mkcd() {
    if [ -z "${1}" ] ; then
        echo "Usage: mkcd PATH"
        echo "mkcd - make a directory and move into it"

        return 1
    else
        mkpath "${*}"

        cd "${*}" || return 1
    fi
}

# Source a file if it exists
source_file() {
    if [ -z "${1}" ] ; then
        echo "Usage: source_file FILE"
        echo "source_file - source FILE if it exists"

        return 1
    else
        if [ -f "${1}" ] ; then
            . "${1}"
        fi
    fi
}


# System

# Failsafe PATH (because '[' and ']' are programs)
PATH="${PATH:-/bin:/sbin:/usr/bin:/usr/local/bin}"

# Source the system profile
source_file "/etc/profile"


# Terminal features

# Disable terminal scroll lock
command_exists stty && stty -ixon


# Environment

# User's name
# Keep this first
USER="${USER:-$(whoami)}"
export USER

# User's identification number
# Keep this second
if [ -z "${UID}" ] ; then
    if command_exists id ; then
        UID="$(id -u "${USER}")"
    fi
fi
export UID

# Auto-set the pager
if [ -z "${PAGER}" ] ; then
    __pager=""

    for __pager in less cat ; do
        if command_exists "${__pager}" ; then
            PAGER="${__pager}"

            break
        fi
    done

    unset __pager
fi
export PAGER

# Ls
# ls colors
_set_dircolors() {
    eval "$(dircolors -b | sed 's/01;05;37;41/0;36/g')"
    #                      ^ also replace blinking red with blue green
}
command_exists dircolors && _set_dircolors

# Shell
# REPL history
mkdir -p "${HOME}/.cache/sh"
HISTFILE="${HOME}/.cache/sh/history"
HISTSIZE="90000"
export HISTSIZE
SAVEHIST="${HISTSIZE}"
export SAVEHIST

# User's tmp directory
# Used for the SSH agent or for XDG_RUNTIME_DIR fallback
if mkpath "/tmp/user/${UID}" && nullwrap chmod 0700 "/tmp/user/${UID}" ; then
    USERTMP="/tmp/user/${UID}"

    nullwrap chmod 0777 /tmp/user
else
    # shellcheck disable=2034
    USERTMP=""
fi

# XDG Base Directory (general failsafe)
# FIXME: XDG_* carries into shells spawned from "su"
XDG_CACHE_HOME="${XDG_CACHE_HOME:-${HOME}/.cache}"
mkpath "${XDG_CACHE_HOME}" && export XDG_CACHE_HOME
XDG_CONFIG_DIRS="${XDG_CONFIG_DIRS:-/etc/xdg}"
mkpath "${XDG_CONFIG_DIRS}" && export XDG_CONFIG_DIRS
XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-${HOME}/.config}"
mkpath "${XDG_CONFIG_HOME}" && export XDG_CONFIG_HOME
XDG_DATA_DIRS="${XDG_DATA_DIRS:-/usr/local/share:/usr/share}"
export XDG_DATA_DIRS
XDG_DATA_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}"
mkpath "${XDG_DATA_HOME}" && export XDG_DATA_HOME
XDG_LOG_HOME="${XDG_LOG_HOME:-${HOME}/.local/var/log}"
mkpath "${XDG_LOG_HOME}" && export XDG_LOG_HOME
XDG_STATE_HOME="${XDG_STATE_HOME:-${HOME}/.local/var/lib}"
mkpath "${XDG_STATE_HOME}" && export XDG_STATE_HOME

# XDG Runtime Directory
# We aim for "/run/user/1000".
__xrd_base=""
for __xrd_base in /run /tmp "${XDG_CACHE_HOME}" ; do
    __xrd="${__xrd_base}/user/${UID}"

    if mkpath "${__xrd}" && nullwrap chmod 0700 "${__xrd}" ; then
        XDG_RUNTIME_DIR="${__xrd}"
        export XDG_RUNTIME_DIR

        nullwrap chmod 0777 "${__xrd}"/../

        break
    fi
done
unset __xrd_base
unset __xrd

# Xorg X11 Server
# Why here? We use XDG_RUNTIME_DIR for Xauthority
XAUTHORITY="${XAUTHORITY:-${XDG_RUNTIME_DIR}/Xauthority}"
export XAUTHORITY
XINITRC="${HOME}/.config/X11/xinitrc"
export XINITRC
XSERVERRC="${HOME}/.config/X11/xserverrc"
export XSERVERRC
rbind startx startxorg 'startx "${XINITRC}" -- "${XSERVERRC}"' -s

# ICE
_x11_cache="${HOME}/.cache/X11"
if mkpath "${_x11_cache}" ; then
    ICEAUTHORITY="${_x11_cache}/ICEauthority"
    export ICEAUTHORITY
fi
unset _x11_cache


# PATH setup

# MyDot manuals
add_to_manpath "${HOME}/.local/share/man/mydot"

# User's manuals
add_to_manpath "${HOME}/.local/share/man"

# We also add old path for compatibility (ie. Cargo & NPM)
# the other one is the one exported by this profile file

# Common programs' homes
add_to_path "/bin"
add_to_path "/sbin"
add_to_path "/usr/bin"
add_to_path "/usr/libexec"
add_to_path "/usr/local/bin"
add_to_path "/usr/local/sbin"
add_to_path "/usr/sbin"

# Selected OPTional programs
add_to_path "/usr/pkg/gnu/bin"

# Cabal (Haskell)
add_to_path "${HOME}/.cabal/bin"

# Nimble (Nim)
add_to_path "${HOME}/.local/share/nimble/bin"

# NPM (Node)
add_to_path "${HOME}/.npm/bin"
add_to_path "${HOME}/.local/share/npm/bin"

# Python
add_to_path "${HOME}/.local/bin"

# Ros(well) (Lisp)
add_to_path "${HOME}/.roswell/bin"

# User's programs
# Keep this last
add_to_path "${HOME}/.bin"
add_to_path "${HOME}/.local/share/mydot/bin"


# Aliases

# NEED_UID0 is used in the following aliases
# Keep this after adding itens to PATH
# If we're root we don't need sudo in most cases (covered here)
if am_i_root || [ -n "${EPREFIX}" ] ; then
    NEED_UID0=""
else
    __need_uid0=""

    for __need_uid0 in doas sudo odus ; do
        if command_exists "${__need_uid0}" ; then
            NEED_UID0="${__need_uid0}"
            a_k_a sudo "${__need_uid0}"

            break
        fi
    done

    unset __need_uid0
fi
export NEED_UID0

# Operating System specific
case "$(uname)" in
    *Linux* )
        alias ls='ls --color=auto --group-directories-first'
        alias l='ls -A'
        alias ll='ls --color=always -Fahl'
        ;;
    * )
        alias l='ls -A'
        alias ll='ls -Fahl'
        ;;
esac

# Files
a_k_a ',,'   'cd ../..'
a_k_a ',,,,' 'cd ../../../..'
a_k_a '..'   'cd ..'
a_k_a '....' 'cd ../..'
a_k_a tf     'tail -fv --retry'
rbind rsync     rcp  'rsync --progress --recursive --stats --verbose' -s
rbind xdg-open  open 'xdg-open' -s
if command_exists tree || command_exists tree-ng ; then
    rbind tree    tree 'tree -F'
    rbind tree-ng tree 'tree-ng -F'
    alias t='tree -I ".git" -L 2 -a'
    alias ta='tree -I ".git" -a'
fi

# Multimedia
rbind ffplay        ffsound 'ffplay -nodisp -hide_banner'
rbind gallery-dl    gyd     'gallery-dl'

# Network
a_k_a no-net-sh         'unshare -r -n ${SH}'
rbind mtr       mtr     'mtr --show-ips --curses'
rbind w3m       w3m     'HOME="${HOME}/.cache" w3m'
rbind arp-scan  seen    '${NEED_UID0} watch arp-scan --localnet'
rbind netstat   seeo    '${NEED_UID0} netstat -acnptu'

# Portage
_alias_portage() {
    alias B='tail -fv "$(portageq envvar PORTAGE_TMPDIR)"/portage/*/*/temp/build.log'
    alias E='tail -fv ${EPREFIX}/var/log/emerge.log'
    alias F='tail -fv ${EPREFIX}/var/log/emerge-fetch.log'

    alias chu='${NEED_UID0} emerge -NUgkv --backtrack=50 --changed-deps --changed-slot --keep-going=y --quiet-build=y --with-bdeps=y @world'
    alias des='${NEED_UID0} emerge -Wv'
    alias pep='${NEED_UID0} emerge -agkv --autounmask --quiet-build=y --keep-going=y'
    alias slr='${NEED_UID0} smart-live-rebuild -- -1 --keep-going=y --usepkg-exclude-live=y'
    alias ewup='${NEED_UID0} emerge -DUgkuv --backtrack=50 --keep-going=y --quiet-build=y --verbose-conflicts --with-bdeps=y @world'
    alias preb='${NEED_UID0} emerge -1 --keep-going=y @preserved-rebuild --usepkg-exclude "*"'
    alias rreb='${NEED_UID0} revdep-rebuild -v -- -1 --keep-going=y --usepkg-exclude "*"'
    alias vmerge='${NEED_UID0} emerge -v --jobs=1 --quiet-build=n'
}
command_exists emerge && _alias_portage

# System tools
rbind tmux T 'tmux attach || tmux'

# Programming
rbind chez       chez       'chez --eehistory "${CHEZ_HISTORY}"'
rbind chezscheme chezscheme 'chezscheme --eehistory "${CHEZ_HISTORY}"'
rbind petite     petite     'petite --eehistory "${CHEZ_HISTORY}"'
rbind chezscheme chez       'chezscheme'          -s
rbind firefox    ff         'firefox'             -s
rbind git        diff-git   'git diff --no-index' -s
rbind ipython    ipy        'ipython'             -s
rbind julia      jl         'julia'               -s
rbind perl       iperl      'perl -de0'           -s
rbind python     py         'python'              -s
rbind python3    python     'python3'             -s

# Shell
a_k_a shell '${SHELL}'
alias so-shrc='source ${HOME}/.profile'
alias so-bashrc='source ${HOME}/.bashrc'
alias so-zshrc='source ${ZDOTDIR}/.zshrc'

# System
a_k_a cpuinfo   'cat /proc/cpuinfo'
a_k_a kerr      '${NEED_UID0} dmesg --level=alert,crit,emerg,err,warn --time-format=iso --color --decode'
a_k_a man-pl    'LANG="pl_PL.UTF-8" man'
a_k_a root      '${NEED_UID0} su -l root'
a_k_a rp        '${NEED_UID0} '
a_k_a running   '( env | sort ; alias ; functions ) 2>/dev/null | ${PAGER}'
rbind grub-mkconfig update-grub '${NEED_UID0} grub-mkconfig -o /boot/grub/grub.cfg' -s

# Busybox
# !!! Keep this last !!!
# If BB is installed, then try to get unavailable programs from it
_alias_busybox() {
    __bb_impl=""

    for __bb_impl in $(busybox --list) ; do
        a_k_a "${__bb_impl}" "busybox ${__bb_impl}"
    done

    unset __bb_impl
}
command_exists busybox && _alias_busybox


# Prompt theme

PS1="SH> "
export PS1

PS2="... "
export PS2


# Additional source

# I guess that GPG and SSH agents
# don't need to be in separate files

# Source additional files
# that are not critical to running the shell
_source_config() {
    __shext=""

    for __shext in "${HOME}/.config/sh/site/"?* ; do
        source_file "${__shext}"
    done

    unset __shext

    return 0
}

if [ -d "${HOME}/.config/sh/site" ] ; then
    _source_config
fi
