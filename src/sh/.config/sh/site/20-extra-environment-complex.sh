#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Aspell
# preferences settings
_aspell_dir="${HOME}/.config/aspell"
if mkpath "${_aspell_dir}" ; then
    _aspell_per_conf="per-conf ${_aspell_dir}/aspell.conf"
    _aspell_personal=""
    _aspell_repl=""

    for _lang in de en pl
    do
        _aspell_personal="${_aspell_personal} ${_aspell_dir}/${_lang}.pws"
        _aspell_repl="${_aspell_repl} ${_aspell_dir}/${_lang}.prepl"
    done

    ASPELL_CONF="per-conf ${_aspell_per_conf} ; personal ${_aspell_personal} ; repl ${_aspell_repl}"
    unset _aspell_per_conf _aspell_personal _aspell_repl
    export ASPELL_CONF
fi
unset _aspell_dir

# CCache (C / C++)
# Exclude root user - breaks ccache in portage
if am_i_root ; then
    unset CCACHE_DIR
else
    # primary config is then ~/.cache/ccache/ccache.conf
    CCACHE_DIR="${HOME}/.cache/ccache"
    export CCACHE_DIR
fi

# Chez
# directory (used by aliases)
_chez_cache="${HOME}/.cache/chez"
if mkpath "${_chez_cache}" ; then
    CHEZ_HISTORY="${_chez_cache}/history"
    export CHEZ_HISTORY
fi
unset _chez_cache

# GTK
# settings
if [ -f "${EPREFIX}/usr/libexec/xdg-desktop-portal" ] ; then
    GTK_USE_PORTAL=1
    export GTK_USE_PORTAL
fi

# Julia
# directory
# exclude root user - breaks Julia build in portage
if am_i_root ; then
    unset JULIA_DEPOT_PATH
else
    JULIA_DEPOT_PATH="${HOME}/.local/share/julia"
    export JULIA_DEPOT_PATH
fi

# NodeJS
# REPL history ; NodeJS can't recursively create it's directory
_nodejs_cache="${HOME}/.cache/npm/node"
if mkpath "${_nodejs_cache}" ; then
    NODE_REPL_HISTORY="${_nodejs_cache}/history"
    export NODE_REPL_HISTORY
fi
unset _nodejs_cache
# NPM config
NPM_CONFIG_USERCONFIG="${HOME}/.config/npm/npmrc"
export NPM_CONFIG_USERCONFIG
# NPM global packages
add_to_path "${HOME}/.local/share/npm/bin"

# Octave
# history file
if am_i_root ; then
    unset OCTAVE_HISTFILE
else
    _octave_cache="${HOME}/.cache/octave"

    if mkpath "${_octave_cache}" ; then
        OCTAVE_HISTFILE="${_octave_cache}/history"
        export OCTAVE_HISTFILE
    fi

    unset _octave_cache
fi

# Python
# Ipython can't recursively create it's directory
_python_cache="${HOME}/.cache/python"
_python_config="${HOME}/.config/python"
_python_data="${HOME}/.local/share/python"
if mkpath "${_python_cache}" ; then
    # pip cache directory
    if am_i_root ; then
        unset PIP_CACHE_DIR
    else
        PIP_CACHE_DIR="${_python_cache}/pip"
        export PIP_CACHE_DIR
    fi

    # pylint directory
    PYLINTHOME="${_python_cache}/pylint"
    export PYLINTHOME
fi
if mkpath "${_python_config}" ; then
    # Jupyter directory
    JUPYTER_CONFIG_DIR="${_python_config}/jupyter"
    export JUPYTER_CONFIG_DIR

    mkpath "${JUPYTER_CONFIG_DIR}"
fi
if mkpath "${_python_data}" ; then
    # Ipython directory
    IPYTHONDIR="${_python_data}/ipython"
    export IPYTHONDIR

    mkpath "${IPYTHONDIR}"
fi
unset _python_cache
unset _python_config
unset _python_data

# Rust
# cargo directory
if am_i_root ; then
    unset CARGO_HOME
else
    CARGO_HOME="${HOME}/.local/share/rust/cargo"
    export CARGO_HOME
    add_to_path "${CARGO_HOME}/bin"
fi

# SQLite
# REPL history
_sqlite_dir="${HOME}/.cache/sqlite"
if mkdir -p "${_sqlite_dir}" ; then
    SQLITE_HISTORY="${_sqlite_dir}/history"
    export SQLITE_HISTORY
fi
unset _sqlite_dir
