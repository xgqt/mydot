#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# ${1} - alias name
# ${2} - directory
# Example:
#   cd_alias etc /etc
#   Means:
#     alias etc="cd /etc && echo * Changed the Directory to etc"
cd_alias() {
    # Check if target directory exits
    if [ -d "${2}" ] ; then
        a_k_a "${1}" "cd ${2} && echo ' * Changed the Directory to' ${2}"
    fi
}

# System
cd_alias P            "${EPREFIX}/etc/portage"
cd_alias conf         "/etc/conf.d"
cd_alias logs         "/var/log"
cd_alias repos        "/var/db/repos"
cd_alias services     "/etc/init.d"
cd_alias src          "/usr/src"
cd_alias tmp          "/tmp"
cd_alias www          "/var/www"

# User - standard
cd_alias data         "${HOME}/Data"
cd_alias desktop      "${HOME}/Desktop"
cd_alias documents    "${HOME}/Documents"
cd_alias downloads    "${HOME}/Downloads"
cd_alias games        "${HOME}/Games"
cd_alias music        "${HOME}/Music"
cd_alias pictures     "${HOME}/Pictures"
cd_alias videos       "${HOME}/Videos"

# User - hidden
cd_alias applications "${HOME}/.local/share/applications"
cd_alias autostart    "${HOME}/.config/autostart"
cd_alias cache        "${HOME}/.cache"
cd_alias config       "${HOME}/.config"
cd_alias data         "${HOME}/.local/share"
cd_alias eprefix      "${EPREFIX}"
cd_alias zcachedir    "${ZCACHEDIR}"
cd_alias zdotdir      "${ZDOTDIR}"
