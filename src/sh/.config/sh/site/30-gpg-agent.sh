#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

GPG_TTY="$(tty)"
export GPG_TTY

PINENTRY_USER_DATA="USE_CURSES=1"
export PINENTRY_USER_DATA

# Proper permissions on ~/.gnupg
_chmod_dot_gpg() {
    if [ -d ~/.gnupg ] ; then
        nullwrap chmod 700 ~/.gnupg
    fi
}

# Start GPG agent if it is not running
_start_agent_gpg() {
    if ! nullwrap pgrep -u "${USER}" gpg-agent ; then
        gpg-agent --daemon --homedir ~/.gnupg --use-standard-socket 2>/dev/null
    fi
}

# Check if GPG agent exists
if ! am_i_root && command_exists gpg-agent ; then
    _chmod_dot_gpg
    _start_agent_gpg
fi
