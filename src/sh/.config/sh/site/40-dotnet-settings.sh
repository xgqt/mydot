#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

DOTNET_SKIP_FIRST_TIME_EXPERIENCE=1
export DOTNET_SKIP_FIRST_TIME_EXPERIENCE

DOTNET_CLI_TELEMETRY_OPTOUT=1
export DOTNET_CLI_TELEMETRY_OPTOUT

POWERSHELL_TELEMETRY_OPTOUT=1
export POWERSHELL_TELEMETRY_OPTOUT

POWERSHELL_UPDATECHECK=0
export POWERSHELL_UPDATECHECK

# Tools
if ! am_i_root ; then
    add_to_path "${HOME}/.dotnet/tools"
fi
