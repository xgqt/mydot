#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

if ! am_i_root && command_exists ocaml && command_exists opam ; then
    # Opam directory
    OPAMROOT="${HOME}/.local/share/ocaml/opam"
    export OPAMROOT

    add_to_path "${OPAMROOT}/default/bin"

    alias opam-activate='eval "$(opam env)"'
    alias opam-deactivate='unset OPAMROOT CAML_LD_LIBRARY_PATH OCAML_TOPLEVEL_PATH OPAM_SWITCH_PREFIX'
fi
