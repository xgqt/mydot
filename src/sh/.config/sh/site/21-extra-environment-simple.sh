#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Conan
# color
CONAN_COLOR_DARK=1
export CONAN_COLOR_DARK
# directory
CONAN_USER_HOME="${HOME}/.local/share/conan"
export CONAN_USER_HOME

# Elixir
# hex directory
HEX_HOME="${HOME}/.local/share/elixir/hex"
export HEX_HOME
# mix directory
MIX_HOME="${HOME}/.local/share/elixir/mix"
export MIX_HOME
add_to_path "${MIX_HOME}"

# Erlang (OTP)
# history file (in ~/.cache/erlang-history)
ERL_AFLAGS="-kernel shell_history enabled"
export ERL_AFLAGS

# GDB
# history file
GDBHISTFILE="${HOME}/.cache/gdb/history"
export GDBHISTFILE

# Go
# directory
GOPATH="${HOME}/.local/share/go"
export GOPATH
add_to_path "${GOPATH}/bin"

# Guile
# history file
GUILE_HISTORY="${HOME}/.cache/guile/history"
export GUILE_HISTORY

# KDE
# data directory, replacement for ~/.kde4
KDEHOME="${HOME}/.config/kde"
export KDEHOME

# Latex
# TeX directories
TEXMFCONFIG="${HOME}/.config/texlive/texmf-config"
export TEXMFCONFIG
TEXMFHOME="${HOME}/.local/share/texlive/texmf"
export TEXMFHOME
TEXMFVAR="${HOME}/.cache/texlive/texmf-var"
export TEXMFVAR

# Less
# Disable less history file
LESSHISTFILE=-
export LESSHISTFILE

# LibDVDcss
# cache directory
DVDCSS_CACHE="${HOME}/.cache/dvdcss"
export DVDCSS_CACHE

# NCurses
# terminfo directories
TERMINFO="${HOME}/.local/share/terminfo"
export TERMINFO
TERMINFO_DIRS="${TERMINFO}:/usr/share/terminfo:${TERMINFO_DIRS}"
export TERMINFO_DIRS

# Nim
# Nimble package directory
NIMBLE_DIR="${HOME}/.local/share/nim/nimble"
export NIMBLE_DIR
add_to_path "${NIMBLE_DIR}/bin"

# Readline
# inputrc location
INPUTRC="${HOME}/.config/readline/inputrc"
export INPUTRC

# Ruby
# gem directories
GEMRC="${HOME}/.config/ruby/gemrc"
export GEMRC
GEM_HOME="${HOME}/.local/share/ruby/gem"
export GEM_HOME
GEM_SPEC_CACHE="${HOME}/.cache/ruby/gem"
export GEM_SPEC_CACHE
add_to_path "${GEM_HOME}/bin"
# solargraph cache directory
SOLARGRAPH_CACHE="${HOME}/.cache/ruby/solargraph"
export SOLARGRAPH_CACHE
