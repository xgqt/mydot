#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# shellcheck disable=2044

if command_exists racket ; then
    _racket_user_dir="${HOME}/.local/share/racket"

    # Racket in PATH
    if [ -d "${_racket_user_dir}" ] ; then
        # We loop to include all different Racket implementation directories
        # ~/.local/share/racket/.racket/7.0/bin or ~/.local/share/racket/.racket/7.7/bin
        for racket_bin_dir in \
            $(find "${_racket_user_dir}" -maxdepth 2 -name bin -type d)
        do
            add_to_path "${racket_bin_dir}"
        done
        unset racket_bin_dir
    fi

    rbind drracket drr 'drracket' -s
    a_k_a rkt 'racket'

    alias rktr5rs='racket -q -I r5rs'
    alias rktquiet='racket --eval "(require xrepl)" --no-init-file --repl'
    alias rktfrtime='rktquiet -I frtime'
    alias rktlazy='rktquiet -I lazy'
    alias rkttyped='rktquiet -I typed/racket'

    unset _racket_user_dir
fi

racket_enter() {
    racket --repl --eval "(enter! (file \"${1}\"))"
}
