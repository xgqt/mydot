#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Proper permissions on ~/.ssh
_chmod_dot_ssh() {
    if [ -d "${HOME}/.ssh" ] ; then
        nullwrap chmod 700 "${HOME}/.ssh"
    fi
}

# Start SSH agent if it is not running
_start_agent_ssh() {
    if [ -n "${USERTMP}" ] ; then
        # start if it is not running
        if ! nullwrap pgrep -u "${USER}" ssh-agent ; then
            ssh-agent > "${USERTMP}/ssh-agent.env"
        fi

        # source environment
        if ! [ -e "${SSH_AUTH_SOCK}" ] ; then
           nullwrap eval "$(cat "${USERTMP}/ssh-agent.env")"
        fi
    fi
}

# Check if SSH agent and XDG runtime directory exist
if ! am_i_root && command_exists ssh-agent && [ -d "${XDG_RUNTIME_DIR}" ] ; then
    # Use ksshaskpass if available.
    _ksshaskpass_path="$(command -v ksshaskpass)"

    if [ -n "${_ksshaskpass_path}" ] ; then
        SSH_ASKPASS="${_ksshaskpass_path}"
        export SSH_ASKPASS
        SSH_ASKPASS_REQUIRE="prefer"  # The magic variable.
        export SSH_ASKPASS_REQUIRE
    fi

    _chmod_dot_ssh
    _start_agent_ssh
fi
