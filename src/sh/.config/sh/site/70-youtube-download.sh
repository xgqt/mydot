#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Youtube-Dl aliases
# Ignore "This expands when defined, not when used. Consider escaping."
# We want that for aliases
# shellcheck disable=2139
# https://github.com/koalaman/shellcheck/wiki/SC2139

if command_exists youtube-dl || command_exists yt-dlp ; then
    _ytd_opts="-i -o '%(title)s.%(ext)s'"

    if command_exists youtube-dl ; then
        alias youtube-dl="youtube-dl ${_ytd_opts}"
    else
        alias youtube-dl="yt-dlp ${_ytd_opts}"
    fi

    alias ytd='youtube-dl'
    alias ytd-album='youtube-dl -f bestaudio -x -o "%(playlist_index)s - %(title)s.%(ext)s"'
    alias ytd-sub-en='youtube-dl --write-srt --sub-lang en'
    alias ytd-sub-pl='youtube-dl --write-srt --sub-lang pl'

    unset _ytd_opts
fi
