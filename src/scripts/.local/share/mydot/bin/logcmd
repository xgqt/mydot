#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

trap 'exit 128' INT

prog_name="$(basename "${0}")"
prog_desc="simple script to be used with cron"
prog_args="[COMMAND]"

usage() {
    cat << USAGE
Usage: ${prog_name} [OPTION] ${prog_args}
${prog_name} - ${prog_desc}

Options:
    -V,      --version        show program version
    -d DIR,  --log-dir DIR    specify the log directory
    -f FILE, --log-file FILE  specify the log file
    -h,      --help           show avalible options

To be safe quote the COMMAND.

Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License
USAGE
}

version() {
    cat <<EOF
${prog_name} 9999

Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License
EOF
}

while [ -n "${1}" ] ; do
    case "${1}" in
        -h | -help | --help )
            usage

            exit 0
            ;;

        -V | -version | --version )
            version

            exit 0
            ;;

        -d | --log-dir )
            log_dir="${2}"

            shift
            ;;

        -f | --log-file )
            log_file="${2}"

            shift
            ;;

        -* )
            version
            echo
            usage

            exit 1
            ;;

        * )
            command_name="$(basename "${1}")"
            command_line="${*}"

            break
            ;;
    esac
    shift
done

if command -v ts >/dev/null 2>&1 ; then
    TS="ts"
elif command -v busybox >/dev/null 2>&1 ; then
    TS="busybox ts"
else
    echo "ERROR: No \"ts\" (timestamp) utility found on the system"

    exit 1
fi

if [ -z "${command_name}" ] ; then
    echo "ERROR: No command given"

    exit 1
fi

if [ -z "${log_dir}" ] ; then
    log_dir="${HOME}/.local/var/log/UserLog"
fi

if [ -z "${log_file}" ] ; then
    log_file="${command_name}.log"
fi

mkdir -p "${log_dir}" || exit 1

eval "${command_line}" 2>&1         \
    | ${TS} ' [%Y.%m.%d %H:%M:%S]'  \
    >> "${log_dir}/${log_file}"
