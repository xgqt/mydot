#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# In cronjob use with > /dev/null

trap 'exit 128' INT
export PATH

prog_name="$(basename "${0}")"
prog_desc="backup a directory"
prog_args="DIRECTORY_TO_BACKUP DIRECTORY_TO_BACKUP_INTO"

usage() {
    cat <<EOF
Usage: ${prog_name} [OPTION]... ${prog_args}
${prog_name} - ${prog_desc}

Options:
    -V, --version  show program version
    -h, --help     show avalible options

Example:
    bak-dir ~/Pictures ~/Data/Backup
EOF
}

version() {
    cat <<EOF
${prog_name} 9999

Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License
EOF
}

count_backups() {
    find "${backup_dir}" -maxdepth 1 -type f -name "${target_base}*.tar.gz" | wc -l
}

tarup() {
    archive_name="${target_base}-$(date +"%Y-%m-%d-%H-%M").tar.gz"

    echo "Creating ${archive_name} in ${backup_dir}"
    tar -zcf "${backup_dir}/${archive_name}" --absolute-names "${target_dir}"
}

rm_old() {
    # shellcheck disable=SC2012
    oldest_archive="$(ls "${backup_dir}"/"${target_base}"* -t1r | sed 1q)"

    echo "Removing ${oldest_archive}"
    rm "${oldest_archive}"
}

case "${1}" in
    -h | -help | --help )
        usage
        exit 0
        ;;
    -V | -version | --version )
        version
        exit 0
        ;;
    "" | -* )
        version
        echo
        usage
        exit 1
        ;;
esac

if [ -z "${2}" ] ; then
    echo "ERROR: Wrong number of arguments"
    exit 1
fi

target_dir="${1}"
target_base="$(basename "${target_dir}")"
backup_dir="${2}"

if [ "$(count_backups)" -lt 3 ] ; then
    tarup
else
    rm_old
    tarup
fi
