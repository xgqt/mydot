#!/usr/bin/env -S awk -F " " -f

# This file is part of mydot.
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

function getTermWidth() {
    defaultWidth = 79;
    termWidth = 0;

    termWidthCmd = "tput cols";
    termWidthCmd | getline termWidth;
    close(termWidthCmd);

    if (!termWidth > 0) {
        termWidth = defaultWidth;
    }

    return termWidth;
}

function getCurrentTime() {
    currentTime = "";

    currentTimeCmd = "date \"+%H:%M:%S\"";
    currentTimeCmd | getline currentTime;
    close(currentTimeCmd);

    return currentTime;
}

function getNinjaPercent(segment) {
    nums[1] = 0;
    nums[2] = 0;

    percent = 0;
    clearedSegment = substr(segment, 2, (length(segment) - 2));

    split(clearedSegment, nums, "/");
    percent = (nums[1] / nums[2]) * 100;

    # Round the return up to 1 digit "after comma".
    # Also, pad to 5 characters.
    return sprintf("%5.1f", percent);
}

function mainLoop() {
    currentTime = getCurrentTime();
    termWidth = getTermWidth();

    currentSeconds = systime();
    diffSeconds = currentSeconds - lastSeconds;

    lastSeconds = currentSeconds;

    diffTimeString = "";

    if (diffSeconds > 60) {
        diffSecondsMinutes = int(diffSeconds / 60);
        diffSecondsRemSeconds = diffSeconds % 60;

        diffTimeString = diffSecondsMinutes "m " diffSecondsRemSeconds "s";
    } else {
        diffTimeString = diffSeconds "s"
    }

    maxWidth = termWidth - currentTimeCharsWidth - 10;

    # Remove "carriage-return" directives.
    sub(/\r/, "");

    out = " " currentTime " " sprintf("%7s", diffTimeString);

    if (match($1, /\[[0-9]+\/[0-9]+\]/)) {
        percent = getNinjaPercent($1);

        out = out percent "%";
        maxWidth = maxWidth - 2 - length(percent);
    }

    trimmedLine = substr($0, 1, maxWidth);
    out = out " " trimmedLine;

    print(out);
}

BEGIN {
    currentTimeCharsWidth = 8;
    lastSeconds = systime();
}

{
    mainLoop();
}
