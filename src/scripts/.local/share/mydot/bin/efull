#!/usr/bin/env bash

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Used here:
#   - bash *
#   - eclean *
#   - eix
#   - emerge (portage) *
#   - genlica
#   - haskell-updater
#   - perl-cleaner
#   - smart-live-rebuild
#   * - mandatory

set -o pipefail
trap 'exit 128' INT

prog_name="$(basename "${0}")"
prog_desc="upgrade your Gentoo box"
prog_args=""

usage() {
    cat <<EOF
Usage: ${prog_name} [OPTION]... ${prog_args}
${prog_name} - ${prog_desc}

Options:
    -n, --no-root  run without elevating privileges
    -V, --version  show program version
    -h, --help     show avalible options
EOF
}

version() {
    cat <<EOF
${prog_name} 9999

Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License
EOF
}

command_exists() {
    if type "${1}" >/dev/null 2>&1 ; then
        return 0
    else
        return 1
    fi
}

main() {
    echo
    echo ">>> Synchronizing the package database"
    if command_exists eix ; then
        eix-sync
        eix-remote update
    else
        emerge --sync
    fi

    echo
    echo ">>> Updating Portage"
    emerge                                      \
        --getbinpkg=y                           \
        --oneshot                               \
        --quiet-build=y                         \
        --update                                \
        --usepkg=y                              \
        --verbose                               \
        sys-apps/portage

    echo
    echo ">>> Rebuilding changed packages"
    emerge                                      \
        --backtrack=50                          \
        --changed-deps                          \
        --changed-slot                          \
        --changed-use                           \
        --getbinpkg=y                           \
        --quiet-build=y                         \
        --usepkg=y                              \
        --verbose                               \
        --with-bdeps=y                          \
        @world

    echo
    echo ">>> Updating the installed packages"
    emerge                                      \
        --backtrack=50                          \
        --changed-use                           \
        --deep                                  \
        --getbinpkg=y                           \
        --quiet-build=y                         \
        --update                                \
        --usepkg=y                              \
        --verbose                               \
        --with-bdeps=y                          \
        @world

    # Next steps are safer to not use built pkgs.
    EMERGE_DEFAULT_OPTS="--oneshot --quiet-build=y --usepkg=n --with-bdeps=y"
    export EMERGE_DEFAULT_OPTS
    FEATURES="-buildpkg -buildpkg-live -buildsyspkg -getbinpkg -test"
    export FEATURES
    PORTAGE_BINHOST=""
    export PORTAGE_BINHOST

    echo
    echo ">>> Regenerating libtool"
    emerge -1 dev-build/libtool

    echo
    echo ">>> Regenerating Perl packages"
    if command_exists perl-cleaner ; then
        perl-cleaner --all
    else
        echo "Not found: perl-cleaner"
        echo "Will skip: Regenerating Perl packages"
    fi

    echo
    echo ">>> Regenerating Haskell packages"
    if command_exists haskell-updater ; then
        haskell-updater -- --usepkg-exclude "*"
    else
        echo "Not found: haskell-updater"
        echo "Will skip: Regenerating Haskell packages"
    fi

    echo
    echo ">>> Rebuilding preserved libraries"
    emerge @preserved-rebuild

    echo
    echo ">>> Updating live packages"
    if command_exists smart-live-rebuild ; then
        smart-live-rebuild --jobs=2
    else
        echo "Not found: smart-live-rebuild"
        echo "Will skip: Updating live packages"
    fi

    echo
    echo ">>> Removing obsolete packages"
    emerge -c

    echo
    echo ">>> Cleaning distribution files"
    eclean-dist --deep --time-limit=3m

    echo
    echo ">>> Cleaning log files"
    emaint logs --clean --time 90

    echo
    echo ">>> Updating environment"
    env-update
}


become_root="yes"

case "${1}" in
    -n | --no-root )
        become_root="no"
        ;;

    -h | --help )
        usage

        exit 0
        ;;

    -V | --version )
        version

        exit 0
        ;;

    -* )
        version
        echo
        usage

        exit 1
        ;;
esac


if [[ "${become_root}" == "yes" ]] && [[ "$(whoami)" != root ]] ; then
    echo "Switching to the root user account"

    su root -c "${0} ${*}"
else
    main "${@}"
fi
