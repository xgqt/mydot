#!/bin/sh


# This file is part of mydot.

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License


trap 'exit 128' INT
export PATH


tmpfile=./disktest.img

prog_name="$(basename "${0}")"
prog_desc="test read and write speed"
prog_args=""


usage() {
    cat <<EOF
Usage: ${prog_name} [OPTION]... ${prog_args}
${prog_name} - ${prog_desc}

Options:
    -V, --version  show program version
    -h, --help     show avalible options
EOF
}

version() {
    cat <<EOF
${prog_name} 9999

Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License
EOF
}

write_drop_val() {
    sysctl -wq vm.drop_caches="${1}"
}

main() {
    echo "Writes:"
    sync
    dd if=/dev/zero of="${tmpfile}" bs=64M count=1 oflag=dsync
    sync

    echo "Reads:"
    write_drop_val "3"
    dd if="${tmpfile}" of=/dev/null bs=64M count=1

    if [ -f "${tmpfile}" ] ; then
        rm "${tmpfile}"
    fi
}


case "${1}" in
    -h | -help | --help )
        usage
        exit 0
        ;;
    -V | -version | --version )
        version
        exit 0
        ;;
    -* )
        version
        echo
        usage
        exit 1
        ;;
esac


if [ "$(whoami)" != root ] ; then
    echo "Switching to the root user account"
    su root -c "${0} ${*}"
else
    main "${@}"
fi
