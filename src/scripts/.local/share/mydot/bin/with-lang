#!/bin/sh


# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.


set -e
trap 'exit 128' INT
export PATH


prog_name="$(basename "${0}")"
prog_desc="run a program with a given lang"
prog_args="<LANG> <SUBCOMMAND> [SUBCOMMAND-ARGS]..."


usage() {
    cat <<EOF
Usage: ${prog_name} [OPTION]... ${prog_args}
${prog_name} - ${prog_desc}

Options:
    -V, --version  show program version
    -h, --help     show avalible options
EOF
}

version() {
    cat <<EOF
${prog_name} 9999

Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License
EOF
}


case "${1}" in
    -h | -help | --help )
        usage
        exit 0
        ;;
    -V | -version | --version )
        version
        exit 0
        ;;
    -* )
        version
        echo
        usage
        exit 1
        ;;
esac


if [ -n "${1}" ] ; then
    lang="${1}"
else
    echo "Error: no <LANG> given!"
    exit 1
fi

shift

if [ -z "${1}" ] ; then
    echo "Error: no <SUBCOMMAND> given!"
    exit 1
fi


LANG="${lang}.UTF-8"
export LANG
LANGUAGE="${lang}.UTF-8"
export LANGUAGE

LC_CTYPE="${lang}.utf8"
export LC_CTYPE
LC_NUMERIC="${lang}.utf8"
export LC_NUMERIC
LC_TIME="${lang}.utf8"
export LC_TIME
LC_MONETARY="${lang}.utf8"
export LC_MONETARY
LC_MESSAGES="${lang}.utf8"
export LC_MESSAGES
LC_PAPER="${lang}.utf8"
export LC_PAPER
LC_NAME="${lang}.utf8"
export LC_NAME
LC_ADDRESS="${lang}.utf8"
export LC_ADDRESS
LC_TELEPHONE="${lang}.utf8"
export LC_TELEPHONE
LC_MEASUREMENT="${lang}.utf8"
export LC_MEASUREMENT
LC_IDENTIFICATION="${lang}.utf8"
export LC_IDENTIFICATION

unset LC_COLLATE


exec "${@}"
