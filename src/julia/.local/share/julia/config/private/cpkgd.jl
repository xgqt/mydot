#!/usr/bin/env julia


# This file is part of mydot.

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License


# CPKGD - Change (to the) PacKaGe Directory


"""
Show the ``pkg``'s directory.

# Examples
```julia-repl
julia> pkgd("OhMyREPL")
```
"""
function pkgd(pkg::String)
    let pkgfile = Base.find_package(pkg)
        if pkgfile === nothing
            @warn "package file of package \"$pkg\" does not exist"
            return nothing
        else
            return dirname(pkgfile)
        end
    end
end

@doc """
Enter the ``pkg``'s directory.

# Examples
```julia-repl
julia> cpkgd("OhMyREPL")
```
"""
function cpkgd(pkg::String)
    let dir = pkgd(pkg)
        if dir !== nothing
            cd(dir)
        end
    end
    println(pwd())
    return nothing
end

@doc """
Enter the ``pkg``'s directory
(macro wrapper for `cpkgd` function).

# Examples
```julia-repl
julia> @cpkgd OhMyREPL
```
"""
macro cpkgd(pkg)
    cpkgd(string(pkg))
end
