#!/usr/bin/env julia


# This file is part of mydot.

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License


# Require


"""
Try to use the ``pkg``, if the package cannot be loaded only print a warning.

This macro exists specially for this ``startup.jl`` file,
so that it is usable regardless of `require`d pkgs being installed or not.

# Examples
```julia-repl
julia> @require OhMyREPL
```
"""
macro require(pkg)
    try
        @eval using $pkg
        return true
    catch err
        @warn "Could not load package: $pkg"
        return false
    end
end
