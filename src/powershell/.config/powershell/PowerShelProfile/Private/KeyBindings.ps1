# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

Import-Module "${PSScriptRoot}/Helpers/AvailableModule.ps1"

function Set-KeyBindings {
    if (Get-AvailableModule PSReadLine) {
        # Automatic suggestions ("forward" to use current suggestion).
        Set-PSReadLineOption -PredictionSource History

        Set-PSReadLineOption -BellStyle None

        # Enable the PSReadLine compeltion menu.
        Set-PSReadlineKeyHandler -Key "Tab" -Function MenuComplete

        # Backward and formward word with Control and arrows.
        Set-PSReadlineKeyHandler -Key "Ctrl+LeftArrow"  -Function BackwardWord
        Set-PSReadlineKeyHandler -Key "Ctrl+RightArrow" -Function ForwardWord

        Set-PSReadlineKeyHandler -Key "Ctrl+a" -Function BeginningOfLine
        Set-PSReadlineKeyHandler -Key "Ctrl+d" -Function DeleteCharOrExit
        Set-PSReadlineKeyHandler -Key "Ctrl+e" -Function EndOfLine
        Set-PSReadlineKeyHandler -Key "Ctrl+u" -Function BackwardKillInput
        Set-PSReadlineKeyHandler -Key "Ctrl+w" -Function UnixWordRubout
    }
}

Set-KeyBindings
