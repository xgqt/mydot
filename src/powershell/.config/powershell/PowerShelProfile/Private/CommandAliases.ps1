# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

function Set-CommandAliases {
    # Remove "type" alias that works like Linux's "cat".
    # Add an alias that works like Linux's "type".
    Remove-Alias -Scope Global -Name type
    Set-Alias -Scope Global -Name type -Value Get-Command

    Set-Alias -Scope Global -Name mkcd -Value New-ItemAndLocation
    Set-Alias -Scope Global -Name .. -Value Set-LocationUp

    if ($IsLinux) {
        Set-Alias -Scope Global -Name ll -Value Get-LinuxLongList
        Set-Alias -Scope Global -Name l  -Value Get-LinuxList
    }
    elseif ($IsWindows) {
        Set-Alias -Scope Global -Name ll -Value Get-NTLongList
        Set-Alias -Scope Global -Name l  -Value Get-NTList
    }
    else {
        Set-Alias -Scope Global -Name ll -Value Get-UnixLongList
        Set-Alias -Scope Global -Name l  -Value Get-UnixList
    }
}

Set-CommandAliases
