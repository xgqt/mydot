# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

Import-Module "${PSScriptRoot}/Helpers/AvailableModule.ps1"

function Install-WantedModules {
    $wantedModules = @(
        "PSReadLine"

        # Docker completion.
        "DockerCompletion"

        # .NET completion.
        "posh-dotnet"

        # Git integration.
        # Also provides tab completion for "git".
        "posh-git"

        # Z port for PowerShell.
        "z"
    )

    $wantedModules.GetEnumerator().ForEach(
        {
            if (Get-AvailableModule $_) {
                [Console]::WriteLine("Module already installed: ${_}")
            }
            else {
                [Console]::WriteLine("Installing module: ${_}")

                Install-Module -Scope CurrentUser -Verbose $_
            }
        }
    )
}
