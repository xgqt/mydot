#!/usr/bin/env zsh


# This file is part of mydot.

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License


zstyle ':vcs_info:*' formats '%F{white}|%F{green}%b'

PROMPT=$'%B%F{cyan}%n%F{white}@%b%M:%F{green}%(7~|%-2~/…/%3~|%~)${vcs_info_msg_0_}
%B%F{white}%(?..%F{red})> %f%b'

RPROMPT=$'%F{cyan}%D{%H:%M:%S %d.%m.%Y}%f%b'
