#!/usr/bin/env zsh

# This file is part of mydot.

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# Based on:
# https://docs.microsoft.com/en-us/dotnet/core/tools/enable-tab-autocomplete

# shellcheck disable=2034,2283

if command_exists dotnet ; then
    _dotnet_zsh_complete() {
        local completions=( "$(dotnet complete "${words}")" )

        # If completion list is empty, then continue with filename selection.
        if [[ -z "${completions[*]}" ]] ; then
            _arguments '*::arguments: _normal'

            return
        fi

        # This is not a variable assignment, don't remove spaces!
        _values = "${(ps:\n:)completions}"
    }

    compdef _dotnet_zsh_complete dotnet
fi
