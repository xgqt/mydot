;;; early-init --- initialization -*- lexical-binding: t -*-


;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License

;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:



;;; Code:


;; Faster runtime: read-process-output

(setq read-process-output-max (* 1024 1024))


;; Start up faster: gc-cons

(setq gc-cons-threshold (* 1024 1024 1024))
(setq gc-cons-percentage 0.8)


;; Start up faster: file-name-handler

(defvar early-init-file-name-handler-alist file-name-handler-alist)

(setq file-name-handler-alist nil)

(defun early-init-revert-file-name-handler-alist ()
  "Revert to default `file-name-handler-alist'."
  (setq file-name-handler-alist early-init-file-name-handler-alist))

(add-hook 'emacs-startup-hook 'early-init-revert-file-name-handler-alist)


(provide 'early-init)



;;; early-init.el ends here
