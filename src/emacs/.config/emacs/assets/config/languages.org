#+TITLE: Languages

#+AUTHOR: Maciej Barć
#+DATE: <2022-01-11 Tue 10:41>
#+LANGUAGE: en

#+STARTUP: content inlineimages
#+OPTIONS: num:nil toc:t
#+REVEAL_THEME: black


#+BEGIN_QUOTE
This file is part of mydot - XGQT's configuration files.
Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License

mydot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

mydot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mydot.  If not, see <https://www.gnu.org/licenses/>.
#+END_QUOTE


#+BEGIN_SRC emacs-lisp
  ;;; languages.el --- -*- lexical-binding: t -*-
#+END_SRC


* Programming

  So-called "real" programming languages :D

** Dotnet

   [[https://docs.microsoft.com/en-us/dotnet/][.NET]] ecosystem support.
   [[https://docs.microsoft.com/en-us/dotnet/csharp/][C#]] language support.
   [[https://docs.microsoft.com/en-us/dotnet/fsharp/][F#]] language support.

   #+BEGIN_SRC emacs-lisp
     (declare-function c-toggle-auto-hungry-state "cc-cmds" (&optional arg))
     (declare-function c-toggle-auto-newline "cc-cmds" (&optional arg))
     (declare-function csharp-mode "csharp-mode" ())

     (use-package csharp-mode
       :if (executable-find "dotnet")
       :mode
       (("\\.cake\\'" . csharp-mode)
        ("\\.cs\\'"   . csharp-mode))
       :config
       (progn
         (c-toggle-auto-hungry-state 1)
         (c-toggle-auto-newline 1)))
   #+END_SRC

   #+BEGIN_SRC emacs-lisp
     (declare-function fsharp-mode "fsharp-mode" ())

     (use-package fsharp-mode
       :if (executable-find "dotnet")
       :hook
       ((fsharp-mode . maybe-enable-lsp))
       :mode
       (("\\.fs\\'"  . fsharp-mode)
        ("\\.fsx\\'" . fsharp-mode))
       :config
       (progn
         (require 'eglot-fsharp nil t)))
   #+END_SRC

** Ebuild

   Gentoo Ebuild language support.

   #+BEGIN_SRC emacs-lisp
     (progn
       (require 'company-ebuild nil t)
       (require 'flycheck-pkgcheck nil t))
   #+END_SRC

** Emacs Lisp

   [[https://www.gnu.org/software/emacs/manual/html_node/elisp/index.html][Emacs Lisp]] interaction configuration.

   #+BEGIN_SRC emacs-lisp
     (use-package ielm
       :commands (ielm)
       :hook
       ((ielm-mode . eldoc-mode)
        (ielm-mode . rainbow-delimiters-mode)))
   #+END_SRC

   Create a =scratch buffer= (with =Elisp interaction mode=).

   #+BEGIN_SRC emacs-lisp
     (defun mydot-make-scratch-buffer ()
       "Create a scratch buffer and switch to it."
       (interactive)
       (switch-to-buffer (get-buffer-create "*scratch*"))
       (lisp-interaction-mode))
   #+END_SRC

** Haskell

   [[https://www.haskell.org/][Haskell]] language support.

   + Important key bindings:
     - =C-c C-l= - haskell-process-load-file (load current file into the REPL)
     - =C-c C-x= - haskell-process-cabal     (run a =cabal= command)

   + Haskell-Mode:
     - https://github.com/haskell/haskell-mode
     - https://melpa.org/#/haskell-mode

   #+BEGIN_SRC emacs-lisp
     (declare-function haskell-mode "haskell-mode" ())
     (declare-function interactive-haskell-mode "haskell" ())

     (use-package haskell-mode
       :if (executable-find "ghc")
       :mode ("\\.ghci\\'" . haskell-mode)
       :hook (haskell-mode . interactive-haskell-mode))
   #+END_SRC

** JS

   [[https://www.javascript.com/][JavaScript]] language support.
   =npm install -g typescript typescript-language-server=

   #+BEGIN_SRC emacs-lisp
     (use-package js
       :mode
       (("\\.js\\'"          . js-mode)
        ("\\.json\\'"        . js-json-mode)
        ("\\.bazel.lock\\'"  . js-json-mode))
       :custom
       ((js-indent-level 2)))
   #+END_SRC

** JVM

   [[https://clojure.org/][Clojure]] language support.

   + Clojure-Mode:
     - https://github.com/clojure-emacs/clojure-mode
     - https://melpa.org/#/clojure-mode

   #+BEGIN_SRC emacs-lisp
     (declare-function clojure-mode "clojure-mode" ())

     (use-package clojure-mode
       :if (executable-find "clojure")
       :mode
       (("\\.clj$" . clojure-mode)
        ("\\.edn$" . clojure-mode)))
   #+END_SRC

** Lisp

   [[https://common-lisp.net/][Lisp]] language support.

   + Sly:
     - http://joaotavora.github.io/sly/
     - https://github.com/joaotavora/sly
     - https://melpa.org/#/sly

   #+BEGIN_SRC emacs-lisp
     (declare-function sly-mrepl-mode "sly-mrepl" ())

     (use-package sly
       :if (executable-find "sbcl")
       :defer t
       :hook ((sly-mrepl-mode . rainbow-delimiters-mode))
       :custom
       ((inferior-lisp-program "sbcl")
        (sly-complete-symbol-function 'sly-simple-completions)
        (sly-mrepl-history-file-name (mydot-expand-user-file "sly-history"))
        (sly-net-coding-system 'utf-8-unix))
       :config
       (progn
         (with-eval-after-load 'sly-mrepl
           (when (boundp 'sly-mrepl-mode-map)
             (define-key sly-mrepl-mode-map
                         (kbd "C-<down>") 'comint-next-input)
             (define-key sly-mrepl-mode-map
                         (kbd "C-<up>")   'comint-previous-input)))))
   #+END_SRC

** Lua

   [[http://www.lua.org/][Lua]] language support with [[http://luajit.org/][LuaJIT]].

   + Lua-Mode:
     - https://github.com/immerrr/lua-mode
     - https://melpa.org/#/lua-mode

   #+BEGIN_SRC emacs-lisp
     (declare-function lua-mode "lua-mode" ())
     (declare-function lua-mode "lua-send-region" (start end))

     (use-package lua-mode
       :if (executable-find "lua")
       :mode (("\\.lua\\'" . lua-mode))
       :bind (:map lua-mode-map ("C-l <return>" . lua-send-region)))
   #+END_SRC

** Perl

   [[https://www.perl.org/][Perl]] language support.

   #+BEGIN_SRC emacs-lisp
     (use-package cperl-mode
       :init
       (progn
         (defalias 'perl-mode 'cperl-mode)))
   #+END_SRC

** Python

   [[https://www.python.org/][Python]] IDE.

   WARNING: Remember to run =elpy-config= to install some necessary packages.

   DEBUG: If =~/.config/emacs/elpy/rpc_venv= is empty remove it and run =elpy-config=.

   + Elpy:
     - https://elpy.readthedocs.io
     - https://github.com/jorgenschaefer/elpy
     - https://melpa.org/#/elpy

   #+BEGIN_SRC emacs-lisp
     (declare-function elpy-enable "elpy" ())

     (use-package elpy
       :if (executable-find "python")
       :hook (python-mode . elpy-enable)
       :custom
       ((elpy-rpc-virtualenv-path (mydot-expand-user-file "elpy/rpc_venv")))
       :config
       (progn
         (setq elpy-modules (delete 'elpy-module-flymake elpy-modules))))
   #+END_SRC

   Start the Python shell with =run-python=.

   Settings for [[https://ipython.org/][IPython]] support.

    #+BEGIN_SRC emacs-lisp
      (use-package python
        :mode ("\\.py\\'" . python-mode)
        :config
        (progn
          (dolist (python-interpreter '("ipython3" "python3"))
            (add-to-list 'python-shell-completion-native-disabled-interpreters
                         python-interpreter))

          (when (executable-find "ipython3")
            (setq python-shell-interpreter "ipython3")
            (setq python-shell-interpreter-args "-i --simple-prompt"))))
    #+END_SRC

    #+begin_src emacs-lisp
      (declare-function pyvenv-workon "pyvenv" ())

      (defun mydot-workon-virtualenvs ()
        "Set WORKON_HOME and switch the active Python virtual environment."
        (interactive)

        (setenv "WORKON_HOME" (expand-file-name "virtualenvs" "~/.local/share"))
        (call-interactively #'pyvenv-workon))
    #+end_src

** R

   [[https://www.r-project.org/][R]] language support.
   Run ESS's R lang REPL with =run-ess-r=.
   Quickly run current line with =C-return=.

   + Ess:
     - https://github.com/emacs-ess/ESS
     - https://melpa.org/#/ess

   #+BEGIN_SRC emacs-lisp
     (declare-function ess-r-mode "ess-r-mode" ())

     (use-package ess
       :if (executable-find "R")
       :mode ("\\.R\\'" . ess-r-mode)
       :config
       (progn
         (setq ess-history-directory (mydot-expand-user-file "ess/"))
         (when (not (file-exists-p ess-history-directory))
           (with-temp-buffer (make-directory ess-history-directory)))))
   #+END_SRC

** Racket

   [[https://racket-lang.org/][Racket]] language support.

   Remember to run [[elisp:(racket-mode-start-faster)][racket-mode-start-faster]]
   ([[https://racket-mode.com/#racket_002dmode_002dstart_002dfaster][docs]]).

   + Important key bindings:
     - =f5=      - racket-run-and-switch-to-repl
     - =C-c C-.= - racket-xp-describe      (documentation in Emacs)
     - =C-c C-d= - racket-xp-documentation (documentation in a browser)
     - =C-c C-l= - racket-logger           (open a logger split)
     - =C-c C-r= - racket-send-region      (evaluate selected region in the REPL)
     - =M-.=     - xref-find-definitions   (from xref)

   + Racket-Mode:
     - https://github.com/greghendershott/racket-mode
     - https://melpa.org/#/racket-mode
     - https://racket-mode.com

   #+BEGIN_SRC emacs-lisp
     (declare-function racket-xp-mode "racket-xp" ())

     (defun mydot-enable-racket-support-modes ()
       "Enable miscellaneous Racket complementary support modes."
       (when (and (buffer-file-name)
                  (string-match "\\.\\(rkt\\|scrbl\\)\\'"
                                (expand-file-name (buffer-file-name))))
         ;; Activate them in "reverse" order, first the Racket-XP-Mode,
         ;; then small modes, then other heavier modes.
         (racket-xp-mode)
         (flycheck-mode)
         (eldoc-mode)))
   #+END_SRC

   #+BEGIN_SRC emacs-lisp
     (declare-function racket-repl-exit "racket-repl" ())

     (defun mydot-racket-repl-kill ()
       "Kill current Racket REPL."
       (interactive)
       (let ((buffer (current-buffer)))
         (cond
          ((equal major-mode 'racket-repl-mode)
           (racket-repl-exit)
           (kill-buffer buffer)
           (delete-window)))))
   #+END_SRC

   #+BEGIN_SRC emacs-lisp
     (declare-function racket--logger-set "racket-logger" (topic level))

     (use-package racket-mode
       :if (executable-find "racket")
       :mode
       (("\\.rkt\\'" . racket-mode)
        ("\\.rkt\\(d\\|l\\)\\'" . racket-mode)
        ("\\.scrbl\\'" . racket-mode)
        ("\\.zuo\\'" . racket-mode))
       :hook
       ((racket-mode . mydot-enable-racket-support-modes)
        (racket-repl-mode . rainbow-delimiters-mode))
       :bind
       (:map racket-mode-map
        ("C-l <return>" . racket-send-region)
        :map racket-repl-mode-map
        ("C-<up>" . racket-repl-previous-input)
        ("C-<down>" . racket-repl-next-input)
        ("C-d" . mydot-racket-repl-kill))
       :custom
       ((racket-show-functions '(racket-show-echo-area)))
       :config
       (progn
         ;; The racket checker in unnecessary
         ;; when using greghendershott's racket-mode.
         (setq-default flycheck-disabled-checkers
                       (append flycheck-disabled-checkers '(racket)))
         ;; Set logging level to "warning".
         (racket--logger-set '* 'warning)
         ;; Alias deprecated functions to overwrite them
         (defalias 'racket-describe 'racket-xp-describe)
         (defalias 'racket-doc      'racket-xp-documentation)))
   #+END_SRC

** Scheme

   [[http://www.scheme-reports.org/][Scheme]] language support.

   Important:
   - =C-c C-s= - switch scheme =implementation=
   - =C-c C-z= - switch to =REPL=

   + Geiser:
     - https://gitlab.com/emacs-geiser/geiser
     - https://melpa.org/#/geiser

   #+BEGIN_SRC emacs-lisp
     (declare-function geiser-mode "geiser-mode" ())

     (use-package geiser
       :hook (scheme-mode . geiser-mode)
       :config
       (progn
         (let ((geiser-dir (mydot-expand-user-file "geiser/")))
           (when (not (file-exists-p geiser-dir))
             (with-temp-buffer (make-directory geiser-dir)))
           (setq geiser-repl-history-filename
                 (concat geiser-dir "history"))
           (when (and (boundp 'geiser-chez-binary)
                      (executable-find "chezscheme"))
             (setq geiser-chez-binary "chezscheme")))))
   #+END_SRC

   Don't use scheme-mode in Racket files.

   #+BEGIN_SRC emacs-lisp
     (setq auto-mode-alist
           (delete '("\\.rkt\\'" . scheme-mode) auto-mode-alist))
   #+END_SRC

** SQL

   [[https://en.wikipedia.org/wiki/SQL][SQL]] language support.

   #+BEGIN_SRC emacs-lisp
     (use-package sql
       :custom
       ((sql-postgres-login-params '((user :default "postgres")
                                     (database :default "postgres")
                                     (server :default "/var/run/postgresql/")
                                     (port :default 5432))))
       :hook
       ((sql-interactive-mode . rainbow-delimiters-mode)
        (sql-interactive-mode . toggle-truncate-lines)))
   #+END_SRC


* Configuration

  Configuration, markup, small DSLs, theoretical, ...

** Web Development

   #+BEGIN_SRC emacs-lisp
     (use-package sgml-mode
       :mode
       (("\\.[agj]sp\\'"     . html-mode)
        ("\\.as[acp]x\\'"    . html-mode)
        ("\\.djhtml\\'"      . html-mode)
        ("\\.ejs\\'"         . html-mode)
        ("\\.erb\\'"         . html-mode)
        ("\\.html?\\'"       . html-mode)
        ("\\.jsp\\'"         . html-mode)))
   #+END_SRC

   #+BEGIN_SRC emacs-lisp
     (use-package nxml-mode
       :mode
       (("\\.props\\'"  . nxml-mode)
        ("\\.xaml\\'" . nxml-mode)
        ("\\.xml\\'"  . nxml-mode)
        ("\\proj\\'"  . nxml-mode))
       :custom
       ((nxml-slash-auto-complete-flag t)))
   #+END_SRC

** Yaml

   [[https://yaml.org][Yaml]] language support.

   + Yaml-Mode:
     - https://github.com/yoshiki/yaml-mode
     - https://melpa.org/#/yaml-mode

   #+BEGIN_SRC emacs-lisp
     (declare-function yaml-mode "yaml-mode" ())

     (use-package yaml-mode
       :mode
       (("\\.sls\\'" . yaml-mode)  ; Saltstack
        ("\\.yaml$"  . yaml-mode)
        ("\\.yml$"   . yaml-mode))
       :bind
       (:map yaml-mode-map ("C-m" . newline-and-indent)))
   #+END_SRC


* Math

  Mathematics, logic, proofs, ...

** Coq

   [[https://coq.inria.fr/][Coq]] proof assistants support.

   + Company-Coq:
     - https://github.com/cpitclaudel/company-coq
     - https://melpa.org/#/company-coq

   #+BEGIN_SRC emacs-lisp
     (declare-function company-coq-mode "company-coq" ())

     (use-package company-coq
       :if (executable-find "coqc")
       :hook (coq-mode . company-coq-mode))
   #+END_SRC

** Octave

   [[https://www.gnu.org/software/octave/index][Octave]] support.
   The Emacs Lisp library is built in to Emacs.
   Start the Octave REPL (Inferior Octave) with =run-octave=.

   #+BEGIN_SRC emacs-lisp
     (use-package octave
       :if (executable-find "octave")
       :hook ((inferior-octave-mode . rainbow-delimiters-mode)))
   #+END_SRC

** Prolog

   [[https://www.swi-prolog.org/][Prolog]] language support.

   #+BEGIN_SRC emacs-lisp
     (use-package prolog
       :defer t
       :hook (prolog-inferior-mode . rainbow-delimiters-mode)
       :bind (:map prolog-mode-map ("C-l <return>" . prolog-consult-region)))
   #+END_SRC
