;;; browse-kill-ring.el --- Browse Kill-Ring -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 23 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "24.4"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Browse Kill-Ring.

;;; Code:

;;;###autoload
(defun browse-kill-ring (kill-ring-selection)
  "Select item KILL-RING-SELECTION from the ‘kill-ring’."
  (interactive
   (list
    (completing-read "Select from kill-ring: "
                     (mapcar (lambda (string-with-properties)
                               (substring-no-properties string-with-properties))
                             kill-ring))))
  (insert kill-ring-selection))

;;;###autoload
(defun browse-kill-ring-clear ()
  "Clear the ‘kill-ring’."
  (interactive)
  (setq kill-ring nil)
  (garbage-collect))

;;;###autoload
(defun browse-kill-ring-bind ()
  "Create `browse-kill-ring' bindings."
  (global-set-key (kbd "<insert>") 'browse-kill-ring)
  (global-set-key (kbd "<insertchar>") 'browse-kill-ring))

;;;###autoload
(add-hook 'after-init-hook 'browse-kill-ring-bind)

(provide 'browse-kill-ring)

;;; browse-kill-ring.el ends here
