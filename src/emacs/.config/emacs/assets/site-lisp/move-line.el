;;; move-line.el --- Move current line up or down -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 23 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "27.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Move current line up or down.

;;; Code:

(defgroup move-line nil
  "Move current line up or down."
  :group 'convenience)

;;;###autoload
(defun move-line-up ()
  "Move current line up."
  (interactive)
  (transpose-lines 1)
  (forward-line -2))

;;;###autoload
(defun move-line-down ()
  "Move current line down."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1))

;;;###autoload
(define-minor-mode move-line-mode
  "Move-Line Minor Mode."
  :group 'move-line
  :global t
  :keymap
  (list (cons (kbd "M-<up>") #'move-line-up)
        (cons (kbd "M-<down>") #'move-line-down)))

;;;###autoload
(add-hook 'after-init-hook 'move-line-mode)

(provide 'move-line)

;;; move-line.el ends here
