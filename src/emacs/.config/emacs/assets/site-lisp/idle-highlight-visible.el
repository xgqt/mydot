;;; idle-highlight-visible.el --- Highlight the word the point is on -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Downloaded from: https://github.com/ignacy/idle-highlight-in-visible-buffers
;; Modifications by: Maciej Barć

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 22 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "24.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Based on idle-highlight-mode but works on all visible buffers.
;;
;; Renamed from "idle-highlight-in-visible-buffers-mode.el".

;;; Code:

(require 'thingatpt)

(defgroup idle-highlight-visible nil
  "Highlight other occurrences in all visible buffers of the word at point."
  :group 'faces)

(defface idle-highlight-visible
  '((t (:inherit highlight)))
  "Face used to highlight other occurrences of the word at point."
  :group 'idle-highlight-visible)

(defcustom idle-highlight-visible-exceptions '("def" "end")
  "List of words to be excepted from highlighting."
  :group 'idle-highlight-visible
  :type '(repeat string))

(defcustom idle-highlight-visible-modes-exceptions
  '(dired-mode minibuffer-mode treemacs-mode)
  "List of modes to be excepted from activating the highlighting."
  :group 'idle-highlight-visible
  :type '(repeat symbol))

(defcustom idle-highlight-visible-idle-time 0.5
  "Time after which to highlight the word at point."
  :group 'idle-highlight-visible
  :type 'float)

(defvar idle-highlight-visible-regexp nil
  "Buffer-local regexp to be idle-highlighted.")

(defvar idle-highlight-visible-global-timer nil
  "Timer to trigger highlighting.")

(defun idle-highlight-visible-buffers-list ()
  "Given a list of buffers, return buffers which are currently visible."
  (let ((buffers '()))
    (walk-windows (lambda (w) (push (window-buffer w) buffers))) buffers))

(defun idle-highlight-visible-unhighlight-word ()
  "Remove highlighting from all visible buffers."
  (save-window-excursion
    (dolist (buffer (idle-highlight-visible-buffers-list))
      (switch-to-buffer buffer)
      (when idle-highlight-visible-regexp
        (unhighlight-regexp idle-highlight-visible-regexp)))
    (setq idle-highlight-visible-regexp nil)))

(defun idle-highlight-visible-highlight-word-at-point ()
  "Highlight the word under the point in all visible buffers."
  (let* ((target-symbol (symbol-at-point))
         (target (symbol-name target-symbol)))
    (when (and target-symbol
               (not (member major-mode
                            idle-highlight-visible-modes-exceptions))
               (not (member target
                            idle-highlight-visible-exceptions)))
      (idle-highlight-visible-unhighlight-word)
      (save-window-excursion
        (dolist (buffer (idle-highlight-visible-buffers-list))
          (switch-to-buffer buffer)
          (setq idle-highlight-visible-regexp
                (concat "\\<" (regexp-quote target) "\\>"))
          (highlight-regexp idle-highlight-visible-regexp
                            'idle-highlight-visible))))))

;;;###autoload
(define-minor-mode idle-highlight-visible-mode
  "Idle-Highlight-Visible Minor Mode."
  :group 'idle-highlight-visible
  (if idle-highlight-visible-mode
      (progn (unless idle-highlight-visible-global-timer
               (setq idle-highlight-visible-global-timer
                     (run-with-idle-timer
                      idle-highlight-visible-idle-time
                      :repeat 'idle-highlight-visible-highlight-word-at-point)))
             (set (make-local-variable
                   'idle-highlight-visible-regexp) nil))
    (idle-highlight-visible-unhighlight-word)))

;; WARNING: You might not want to load this like the following.

;;;###autoload
(add-hook 'after-init-hook 'idle-highlight-visible-mode)

(provide 'idle-highlight-visible)

;;; idle-highlight-visible.el ends here
