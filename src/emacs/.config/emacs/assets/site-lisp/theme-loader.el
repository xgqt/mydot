;;; theme-loader.el --- Automatically load a theme -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 22 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "24.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Automatically load a Emacs theme (color scheme).

;;; Code:

(defgroup theme-loader nil
  "Theme loader."
  :group 'emacs)

(defconst theme-loader-default-themes
  '(spacemacs-dark    ; nashamri/spacemacs-theme
    vscode-dark-plus  ; ianyepan/vscode-dark-plus-emacs-theme
    doom-dark+        ; doomemacs/themes
    tsdh-dark)
  "Default list of themes to try to load.")

(defcustom theme-loader-themes theme-loader-default-themes
  "List of themes to try to load."
  :type '(repeat symbol)
  :group 'theme-loader)

(defcustom theme-loader-selected-theme nil
  "Selected theme to pick first.
If non-nil, then it is loaded instead of performing
`theme-loader-default-themes' iteration."
  :type 'symbol
  :group 'theme-loader)

(defcustom theme-loader-mode-line-fixup t
  "Whether to \"fix\" the mode-line in GUI."
  :type 'boolean
  :group 'theme-loader)

(defcustom theme-loader-box-line-width 5
  "Box line width.

Used in `theme-loader--mode-line-fixup'."
  :type 'number
  :group 'theme-loader)

(defvar theme-loader--selected-theme-override nil)

(defun theme-loader--load-available-theme (themes)
  "Load first theme from THEMES list if it is available."
  (let ((theme (car themes))
        (rest-temes (cdr themes)))
    (cond
     ;; Found a theme to load.
     ((member theme (custom-available-themes))
      (load-theme theme t)
      t)
     ;; ‘themes’ was exhausted.
     ((null rest-temes)
      nil)
     ;; Try to load next theme.
     (t
      (theme-loader--load-available-theme rest-temes)))))

(defun theme-loader--mode-line-fixup ()
  "Fix the look of mode-line."
  (custom-set-faces
   `(mode-line
     ((t (:box (:line-width ,theme-loader-box-line-width
                            :color ,(face-background 'mode-line)))))))
  (custom-set-faces
   `(mode-line-inactive
     ((t (:box (:line-width ,theme-loader-box-line-width
                            :color ,(face-background 'mode-line-inactive))))))))

;;;###autoload
(defun theme-loader-load-theme ()
  "Load custom theme from a predefined set of themes."
  (let ((theme (getenv "EMACS_THEME")))
    (cond
     ((member theme '("" "0" "none"))
      (message "Not loading any theme"))
     (theme
      (load-theme (intern theme) t))
     (theme-loader--selected-theme-override
      (load-theme theme-loader--selected-theme-override t))
     (theme-loader-selected-theme
      (load-theme theme-loader-selected-theme t))
     (t
      (theme-loader--load-available-theme theme-loader-themes))))
  (when (and window-system theme-loader-mode-line-fixup)
    (theme-loader--mode-line-fixup)))

;;;###autoload
(defun theme-loader-cmdline (&optional switch)
  "Wrapper for selecting a theme from command line (via Emacs SWITCH)."
  (let ((theme (pop command-line-args-left)))
    (message "theme-loader-cmdline: %s %s" switch theme)
    (setq theme-loader--selected-theme-override (intern theme))
    (theme-loader-load-theme)))

;;;###autoload
(defun theme-loader-add-command-switch ()
  "Add \"--theme\" switch to call `theme-loader-cmdline'."
  (add-to-list 'command-switch-alist '("--theme" . theme-loader-cmdline)))

;;;###autoload
(add-hook 'after-init-hook 'theme-loader-add-command-switch)

;;;###autoload
(add-hook 'after-init-hook 'theme-loader-load-theme)

(provide 'theme-loader)

;;; theme-loader.el ends here
