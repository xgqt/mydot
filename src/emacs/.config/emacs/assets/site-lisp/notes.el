;;; notes.el --- Notes buffer -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 22 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "24.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Notes buffer.

;;; Code:

(defun notes--notes-buffer-available-p ()
  "Check if a notes buffer exists."
  (and (get-buffer "*notes*") t))

(defun notes--generate-new-notes-buffer ()
  "Create a notes buffer."
  (let ((notes-buffer (generate-new-buffer "*notes*")))
    (with-current-buffer notes-buffer
      (set-buffer-major-mode notes-buffer))))

;;;###autoload
(defun notes-generate-notes-buffer ()
  "Create a notes buffer if it does not exist."
  (interactive)
  (cond
   ((notes--notes-buffer-available-p)
    (message "Notes buffer already exists."))
   (t
    (notes--generate-new-notes-buffer))))

;;;###autoload
(defun notes ()
  "Switch to a notes buffer."
  (interactive)
  (notes-generate-notes-buffer)
  (switch-to-buffer "*notes*"))

;;;###autoload
(add-hook 'after-init-hook
          'notes-generate-notes-buffer)

(provide 'notes)

;;; notes.el ends here
