;;; mydot-buffers.el --- Functions to operate on buffers -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: Sep 6 2024
;; Version: 0.0.0
;; Keywords: convenience local
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "24.4"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Functions to operate on buffers.
;;
;; Use with mydot: https://gitlab.com/xgqt/mydot/

;;; Code:

(require 'ibuffer)

(defun mydot-buffers--get-buffer-major-mode (buffer)
  "Return the major-mode of a BUFFER."
  (with-current-buffer buffer
    major-mode))

(defun mydot-buffers--get-major-modes ()
  "Return major-modes of all active buffers."
  (let* ((buffers
          (buffer-list))
         (major-modes
          (mapcar #'mydot-buffers--get-buffer-major-mode buffers)))
    (delete-dups (sort major-modes))))

(defun mydot-buffers--get-ibuffer ()
  "Get the active Ibuffer if it exists."
  (get-buffer "*Ibuffer*"))

(defun mydot-buffers--revert-ibuffer ()
  "Revert the active Ibuffer."
  (let ((buffer
         (mydot-buffers--get-ibuffer)))
    (when buffer
      (with-current-buffer buffer
        (call-interactively #'ibuffer-update)))))

(defun mydot-buffers--kill-mode-buffers (selected-major-mode)
  "Kill all buffers of SELECTED-MAJOR-MODE."
  (mapc (lambda (buffer)
          (let ((buffer-major-mode
                 (mydot-buffers--get-buffer-major-mode buffer)))
            (when (equal buffer-major-mode selected-major-mode)
              (kill-buffer buffer))
            nil))
        (buffer-list)))

;;;###autoload
(defun mydot-buffers-kill-mode-buffers (selected-major-mode)
  "Kill all buffers of SELECTED-MAJOR-MODE.
Select mode interactively."
  (interactive
   (let* ((major-modes
           (mydot-buffers--get-major-modes))
          (selected-major-mode-string
           (completing-read "Active major mode: "
                            major-modes
                            nil
                            'REQUIRE-MATCH)))
     (list (intern selected-major-mode-string))))
  (mydot-buffers--kill-mode-buffers selected-major-mode)
  (mydot-buffers--revert-ibuffer))

;;;###autoload
(defun mydot-buffers-kill-magit-buffers ()
  "Kill all buffers of magit and related modes."
  (interactive)
  (mapc #'mydot-buffers--kill-mode-buffers
        '(magit-diff-mode
          magit-log-mode
          magit-process-mode
          magit-revision-mode
          magit-status-mode))
  (mydot-buffers--revert-ibuffer))

;;;###autoload
(defun mydot-buffers-kill-lingering ()
  "Kill some lingering / leftover buffers.
Calling this will kill: `Info-mode', `dired-mode', `help-mode' and all
magit-mode buffers."
  (interactive)
  (mapc #'mydot-buffers--kill-mode-buffers
        '(Info-mode
          dired-mode
          help-mode))
  (mydot-buffers-kill-magit-buffers))

;; Provide
(provide 'mydot-buffers)

;;; mydot-buffers.el ends here
