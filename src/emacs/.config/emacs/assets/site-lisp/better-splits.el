;;; better-splits.el --- Better splits -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 22 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "28.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Better splits.

;;; Code:

(defgroup better-splits nil
  "Better splits."
  :group 'convenience)

;;;###autoload
(defun better-splits-split-and-follow-horizontally ()
  "Split and follow horizontally."
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))

;;;###autoload
(defun better-splits-split-and-follow-vertically ()
  "Split and follow vertically."
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))

;;;###autoload
(defun better-splits-kill-and-remove-split ()
  "Kill and remove split."
  (interactive)
  (kill-buffer)
  (when (length> (window-list) 1)
    (delete-window)
    (balance-windows)
    (other-window 1)))

;;;###autoload
(define-minor-mode better-splits-mode
  "Better-splits minor mode."
  :global t
  :keymap
  (list (cons (kbd "C-x 2") 'better-splits-split-and-follow-horizontally)
        (cons (kbd "C-x 3") 'better-splits-split-and-follow-vertically)
        (cons (kbd "C-x x") 'better-splits-kill-and-remove-split))
  :group 'better-splits)

;;;###autoload
(add-hook 'after-init-hook 'better-splits-mode)

(provide 'better-splits)

;;; better-splits.el ends here
