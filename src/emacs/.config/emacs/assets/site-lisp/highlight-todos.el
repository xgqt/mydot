;;; highlight-todos.el --- Highlight TODOs -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 22 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "24.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Highlight TODOs.

;;; Code:

(defgroup highlight-todos nil
  "Highlight TODOs."
  :group 'emacs)

(defconst highlight-todos-default-keywords
  '("BUG"
    "CONSIDER"
    "DEBUG"
    "FIXME"
    "HACK"
    "NOTE"
    "NOTICE"
    "TODO"
    "WARNING"
    "WORKAROUND")
  "Default list of TODO keywords to highlight.")

(defcustom highlight-todos-keywords highlight-todos-default-keywords
  "List of TODO keywords to highlight.
Defaults to ‘highlight-todos-default-keywords’."
  :type '(repeat string)
  :group 'highlight-todos)

;;;###autoload
(defun highlight-todos-set-font-lock ()
  "Highlight TODOs.
Uses ‘highlight-todos-keywords’."
  (let ((keywords
         (mapcar #'(lambda (str)
                     `(,(concat str ":")
                       0
                       font-lock-warning-face
                       t))
                 highlight-todos-keywords)))
    (font-lock-add-keywords nil keywords)))

;;;###autoload
(add-hook 'find-file-hook
          'highlight-todos-set-font-lock)

(provide 'highlight-todos)

;;; highlight-todos.el ends here
