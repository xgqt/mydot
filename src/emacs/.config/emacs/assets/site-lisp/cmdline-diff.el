;;; cmdline-diff.el --- Diff from command line -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 22 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "24.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Use Emacs as a diff tool from the command line.
;;
;; Example usage: "emacs --diff fileA.txt fileB.txt".

;;; Code:

;;;###autoload
(defun cmdline-diff (&optional switch)
  "Wrapper for diffing files with ediff from command line (via Emacs SWITCH)."
  (let ((fileA (pop command-line-args-left))
        (fileB (pop command-line-args-left)))
    (message switch)
    (ediff fileA fileB)))

;;;###autoload
(defun cmdline-diff-add-command-switch ()
  "Add \"--diff\" switch to call `cmdline-diff'."
  (add-to-list 'command-switch-alist '("--diff" . cmdline-diff)))

;;;###autoload
(add-hook 'after-init-hook 'cmdline-diff-add-command-switch)

(provide 'cmdline-diff)

;;; cmdline-diff.el ends here
