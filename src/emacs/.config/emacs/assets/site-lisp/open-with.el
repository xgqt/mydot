;;; open-with.el --- Open a GUI system program -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 27 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "24.3"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Open a GUI system program using current buffer's directory / file path.

;;; Code:

(require 'dired)

(defun open-with--buffer-mode (buffer)
  "Return major mode of BUFFER."
  (with-current-buffer buffer
    major-mode))

(defun open-with--dired-buffer-path (buffer)
  "Return the path to current Dired BUFFER's directory."
  (with-current-buffer buffer
    (dired-current-directory)))

(defun open-with--buffer-path (buffer as-directory)
  "Return the path to current BUFFER's directory or file.

Optional argument AS-DIRECTORY will open the directory containing a given file
 if the buffer points to a file path."
  (let ((file-path
         (buffer-file-name buffer)))
    (cond
     ((equal 'dired-mode (open-with--buffer-mode buffer))
      (open-with--dired-buffer-path buffer))
     ((and file-path as-directory)
      (file-name-directory (buffer-file-name buffer)))
     (file-path)
     (t
      (user-error "Buffer is not associated with any directory or file")))))

(defun open-with--execute (executable &rest args)
  "Execute EXECUTABLE with ARGS."
  (let ((command
         (format "%s %s" executable (mapconcat 'identity args " "))))
    (start-process-shell-command executable nil command)))

(defun open-with--execute-default (executable &optional as-directory)
  "Execute EXECUTABLE.

Optional argument AS-DIRECTORY is passed to `open-with--buffer-path' function."
  (open-with--execute executable (open-with--buffer-path (current-buffer) as-directory)))

;;;###autoload
(defun open-with ()
  "Open buffer with a chosen program."
  (interactive)
  (open-with--execute-default (read-string "Program: ")))

;;;###autoload
(defun open-with-azure-studio ()
  "Open buffer with Azure Data Studio."
  (interactive)
  (open-with--execute-default "azuredatastudio"))

;;;###autoload
(defun open-with-vscode ()
  "Open buffer with VSCode."
  (interactive)
  (open-with--execute-default "vscode"))

;;;###autoload
(defun open-with-dolphin ()
  "Open buffer with Dolphin."
  (interactive)
  (open-with--execute-default "dolphin" 'AS-DIRECTORY))

;;;###autoload
(defun open-with-kate ()
  "Open buffer with Kate."
  (interactive)
  (open-with--execute-default "kate"))

;;;###autoload
(defun open-with-kompare (filename)
  "Open buffer with Kompare.

Diff current file against selected FILENAME."
  (interactive "FFile to diff against: ")
  (open-with--execute "kompare"
                      filename
                      (open-with--buffer-path (current-buffer) nil)))

;;;###autoload
(defun open-with-konsole ()
  "Open buffer with Konsole."
  (interactive)
  (open-with--execute "konsole"
                      "--workdir"
                      (open-with--buffer-path (current-buffer) nil)))

;;;###autoload
(defun open-with-kwrite ()
  "Open buffer with Kwrite."
  (interactive)
  (open-with--execute-default "kwrite"))

;;;###autoload
(defun open-with-sourcegit ()
  "Open buffer with Sourcegit."
  (interactive)
  (open-with--execute-default "sourcegit" 'AS-DIRECTORY))

;; TODO: Does not work
;; ;;;###autoload
;; (defun open-with-anything ()
;;   "Open buffer with xdg-open."
;;   (interactive)
;;   (open-with--execute-default "xdg-open"))

(provide 'open-with)

;;; open-with.el ends here
