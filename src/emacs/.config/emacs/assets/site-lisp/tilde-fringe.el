;;; tilde-fringe.el --- Tilde fringe indicator for blank lines -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 23 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "27.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Tilde fringe indicator for blank lines.

;;; Code:

;;;###autoload
(defun tilde-fringe-setup ()
  "Tilde fringe setup."
  (define-fringe-bitmap 'tilde-fringe [0 0 0 113 219 142 0 0] nil nil 'center)
  (setcdr (assq 'empty-line fringe-indicator-alist) 'tilde-fringe)
  (setq-default indicate-empty-lines t))

;;;###autoload
(when window-system
  (add-hook 'after-init-hook
            'tilde-fringe-setup))

(provide 'tilde-fringe)

;;; tilde-fringe.el ends here
