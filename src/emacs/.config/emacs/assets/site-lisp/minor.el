;;; minor.el --- Small tweaks that do not require packages -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: Apr 27 2021
;; Version: 0.0.0
;; Keywords: convenience local
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "24.4"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Some small tweaks that do not require (m)elpa packages.
;;
;; Use with mydot: https://gitlab.com/xgqt/mydot

;;; Code:

(require 'autorevert)
(require 'dired)
(require 'elec-pair)
(require 'ibuffer)
(require 'package)
(require 'time)

;; Editing

;; Auto reloading of buffers.
;;;###autoload
(add-hook 'after-init-hook 'global-auto-revert-mode)

;; Automatically close brackets.
;;;###autoload
(add-hook 'after-init-hook 'electric-pair-mode)

;; Column number display in the mode line.
;;;###autoload
(add-hook 'after-init-hook 'column-number-mode)

;; Display line numbers only in files.
;;;###autoload
(add-hook 'find-file-hook 'display-line-numbers-mode)

;; Highlighting of current line.
;;;###autoload
(add-hook 'after-init-hook 'global-hl-line-mode)

;; Highlight parenthesis.
;;;###autoload
(add-hook 'after-init-hook 'show-paren-mode)

;; Graphics

;; Mode Line
;; Time/load
;;;###autoload
(add-hook 'after-init-hook 'display-time-mode)
;; Display current buffer size.
;;;###autoload
(add-hook 'after-init-hook 'size-indication-mode)

;; Set custom global configuration

;;;###autoload
(defun minor-customize-variables ()
  "Set custom variables."

  ;; Kill processes before existing.
  (setq confirm-kill-processes nil)

  ;; Disable lock files.
  (setq create-lockfiles nil)

  ;; Disable auto-save.
  (setq auto-save-default nil)

  ;; Brackets pairs
  (setq electric-pair-pairs
        '((?\" . ?\")
          (?\( . ?\))
          (?\[ . ?\])
          (?\{ . ?\})
          (?\‘ . ?\’)
          (?\“ . ?\”)))

  ;; Disable backups.
  (setq make-backup-files nil)

  ;; Set encoding to UTF-8.
  (setq locale-coding-system   'utf-8)
  (prefer-coding-system        'utf-8)
  (set-keyboard-coding-system  'utf-8)
  (set-selection-coding-system 'utf-8)
  (set-terminal-coding-system  'utf-8)

  ;; But set time format to "C" for universal English timestamps in ORG mode.
  (setq system-time-locale "C")

  ;; Cursor.
  (setq blink-cursor-blinks 5)

  ;; Tabs
  (setq-default indent-tabs-mode nil)
  (setq-default tab-width 4)

  ;; Dired
  ;; Closing parenthesis to change to wdired mode.
  (define-key dired-mode-map (kbd ")") 'wdired-change-to-wdired-mode)
  (setq global-auto-revert-non-file-buffers t)

  ;; Buffer switching
  ;; in addition to =C-left=, =C-right= & =C-x b=.
  (global-set-key (kbd "C-<next>")  'previous-buffer)
  (global-set-key (kbd "C-<prior>") 'next-buffer)

  ;; For my lovely Polish keyboard.
  ;; TODO: Still needed in $CURRENT_YEAR?
  (define-key key-translation-map (kbd "←") (kbd "M-y"))

  ;; Frame - make/delete.
  (global-set-key (kbd "C-x <next>")  'delete-frame)
  (global-set-key (kbd "C-x <prior>") 'make-frame)

  ;; Ibuffer
  (global-set-key (kbd "C-x C-b") 'ibuffer)
  ;; Kill buffers (=d= & =x=) from ibuffer without confirmation.
  (setq ibuffer-expert t)

  ;; Lowercase and uppercase
  ;; =C-x C-l= to convert a region to lowercase (downcase).
  (put 'downcase-region 'disabled nil)
  ;; =C-x C-u= to convert a region to uppercase.
  (put 'upcase-region   'disabled nil)

  ;; Some terminals (or connections, ie. mosh) set <end> as <select>.
  ;; So, if <select> is not bound - bind it to move-end-of-line.
  (when (not (global-key-binding (kbd "<select>")))
    (global-set-key (kbd "<select>") 'move-end-of-line))

  ;; Zoom with Scroll.
  ;; Control & Scroll Up - Increase.
  (global-set-key (kbd "C-<mouse-4>") 'text-scale-increase)
  ;; Control & Scroll Down - Decrease.
  (global-set-key (kbd "C-<mouse-5>") 'text-scale-decrease)

  ;; Disable =C-t=.
  ;; Used in Tmux, unbind it to not accidentally pass it to Emacs.
  (global-unset-key (kbd "C-t"))

  ;; Disable suspending Emacs with =C-z= and bind it to undo.
  (global-unset-key (kbd "C-z"))
  (global-unset-key (kbd "C-x C-z"))
  (global-set-key   (kbd "C-z") 'undo)

  ;; Disable =C-v=.
  (global-unset-key (kbd "C-v"))

  ;; Disable menu-bar in the console.
  (unless (display-graphic-p)
    (menu-bar-mode -1))

  ;; Display system load without date-time.
  (setq display-time-format "" display-time-load-average-threshold '0.01)

  ;; Pass "y or n" instead of "yes or no".
  (defalias 'yes-or-no-p 'y-or-n-p)

  ;; Do not inform about redefinitions.
  ;; Thanks to: https://andrewjamesjohnson.com/suppressing-ad-handle-definition-warnings-in-emacs/
  (setq ad-redefinition-action 'accept)

  ;; Setup of Emacs's packages.
  (setq package-archives
        '(("elpa"   . "https://tromey.com/elpa/")
          ("gnu"    . "https://elpa.gnu.org/packages/")
          ("melpa"  . "https://melpa.org/packages/")
          ("xgqt"   . "https://xgqt.gitlab.io/emacs-gitlab-elpa/packages/")))

  ;; Initialize package module.
  (package-initialize)

  t)

;;;###autoload
(add-hook 'after-init-hook 'minor-customize-variables)

;; Mode specific configuration

;; No forced newlines in diffs
;;;###autoload
(add-hook 'diff-mode-hook
          (lambda () (setq-local require-final-newline nil)))

(provide 'minor)

;;; minor.el ends here
