;;; commitizen.el --- Commit creation support with Commitizen -*- lexical-binding: t -*-

;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;;
;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.

;; Authors: Maciej Barć <xgqt@xgqt.org>
;; Created: 15 May 2024
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/mydot
;; Package-Requires: ((emacs "24.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later

;;; Commentary:

;; Commit creation support via Commitizen.

;;; Code:

(require 'term)

;;;###autoload
(defun commitizen-commit ()
  "Create a commit with Commitizen."
  (interactive)
  (let* ((cz-args
          '("commit" "--all" "--" "--signoff"))
         (buffer
          (apply #'make-term "commitizen-commit" "cz" nil cz-args)))
    (when (buffer-live-p buffer)
      (with-current-buffer buffer
        (term-char-mode)
        (pop-to-buffer-same-window buffer)))))

(provide 'commitizen)

;;; commitizen.el ends here
