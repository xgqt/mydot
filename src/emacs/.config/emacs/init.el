;;; init --- initialization -*- lexical-binding: t -*-


;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License

;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:


;; Delete the ~/.emacs and ~/.emacs.d directories and place this file
;; in the ~/.config/emacs directory.



;;; Code:


;;; Functions for loading components

(defun mydot-load-org-file (file)
  "Load a org FILE into the running Emacs."
  (org-babel-load-file (expand-file-name file)))

(defun mydot-expand-user-file (file)
  "Return a path to FILE prepended with `user-emacs-directory'."
  (expand-file-name file user-emacs-directory))

(defun mydot-load-either (file)
  "Load FILE that is found in `user-emacs-directory' or current directory."
  (let ((user-file (mydot-expand-user-file file))
        (cwd-file (expand-file-name file ".")))
    (cond
     ((file-readable-p user-file)
      (mydot-load-org-file user-file))
     ((file-readable-p cwd-file)
      (mydot-load-org-file cwd-file))
     (t
      (message "Unable to load file %s, searched for %s and %s" file user-file cwd-file)))))

(defun mydot-warn (string &rest objects)
  "Display a warning message formatting STRING with OBJECTS."
  (display-warning 'mydot (apply #'format string objects) :warning)
  nil)


;; Packages

(defvar mydot--init-requirements
  (append (if (or (display-graphic-p) (getenv "DISPLAY"))
              '(all-the-icons
                all-the-icons-dired
                all-the-icons-ibuffer
                all-the-icons-ivy-rich)
            '())
          '(amx
            company
            counsel
            dashboard
            diff-hl
            diminish
            editorconfig
            eglot
            flycheck
            highlight-indentation
            ivy-rich
            markdown-mode
            org-appear
            projectile
            rainbow-delimiters
            swiper
            switch-window
            undo-tree
            use-package
            which-key
            yasnippet
            yasnippet-snippets)))

(defun mydot-install-requirements ()
  "Refresh and install required packages."
  (interactive)
  (package-refresh-contents)
  (mapc (lambda (pkg)
          (let ((require-result (require pkg nil t))
                (pkg-name (symbol-name pkg)))
            (if (equal require-result pkg)
                (mydot-warn "Already installed: %s" pkg-name)
              (mydot-warn "Not yet installed: %s" pkg-name)
              (package-install pkg))))
        mydot--init-requirements))


;; Load custom components

(defgroup mydot-emacs nil
  "MyDot GNU Emacs customization group."
  :group 'emacs)

;; Set path to store "custom-set".
(setq custom-file (mydot-expand-user-file "emacs-custom.el"))

(defconst pre-init-file
  (mydot-expand-user-file "pre-init.el")
  "Custom init file loaded while loading init.el.
Defined by MyDot Emacs configuration.")

(when (file-readable-p pre-init-file)
  (load pre-init-file))

;; Custom site-lisp
(defvar mydot-site-lisp
  (mydot-expand-user-file "assets/site-lisp"))

(add-to-list 'load-path mydot-site-lisp)

(defvar mydot-site-lisp-autoloads
  (expand-file-name "autoloads.el" mydot-site-lisp))

(when (file-readable-p mydot-site-lisp-autoloads)
  (require 'autoloads mydot-site-lisp-autoloads))

;; Load minor
;; Some small tweaks that do not require packages.
(unless (require 'minor nil t)
  (mydot-warn "Package \"minor\" not loaded!"))

(defun mydot-recompile-site-lisp ()
  "Recompile MyDot's \"site-lisp\" directory."
  (interactive)

  (message "Recompiling whole %s directory" mydot-site-lisp)
  (byte-recompile-directory mydot-site-lisp 0)

  (message "Regenerating autolaods %s" mydot-site-lisp-autoloads)
  (cond
   ((fboundp 'make-directory-autoloads)
    (make-directory-autoloads mydot-site-lisp mydot-site-lisp-autoloads))
   (t
    (update-directory-autoloads mydot-site-lisp-autoloads))))

(defun mydot-recompile-config ()
  "Recompile MyDot's \"config\" directory."
  (interactive)
  (message "Recompiling whole %s directory" (mydot-expand-user-file "assets/config"))
  (byte-recompile-directory (mydot-expand-user-file "assets/config") 0))

;; This is the actual config file,
;; it is omitted if it doesn't exist so emacs won't refuse to launch.
(defun mydot-config-load ()
  "Load required MyDot ORG configuration files."
  (interactive)
  (mapc (lambda (f)
          (mydot-load-either (concat "./assets/config/" f ".org")))
        '("environment" "appearance" "languages" "org" "misc"))
  t)

;; Load the full configuration.
(let ((org (getenv "EMACS_ORG")))
  (cond
   ((equal org "1") (mydot-config-load))
   ((equal org "0")
    (message "EMACS_ORG is set to 0, not loading the ORG config"))
   ((or (equal (getenv "UID") "0") (equal (getenv "USER") "root"))
    (message "Running under root, not loading the ORG config."))
   (t
    (mydot-config-load))))

(when (file-readable-p custom-file)
  (load custom-file))



;;; init.el ends here
