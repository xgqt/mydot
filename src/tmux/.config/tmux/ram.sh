#!/bin/sh

# This file is part of mydot.
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later

if command -v vmstat >/dev/null 2>&1 ; then
    vmstat_out="$(LC_ALL=C vmstat -s)"
else
    vmstat_out=""
fi

memory="$(echo "${vmstat_out}" | grep -i "free memory" | awk -F " " '{ print $1 }')"

gbs=$((memory / 1048576))
mbs=$((memory / 1024))
kbs=$((memory))

if [ "${memory}" != "" ] ; then
    if [ "${gbs}" != 0 ] ; then
        printf "%sG" "${gbs}"
    elif [ "${mbs}" != 0 ] ; then
        printf "%sM" "${mbs}"
    elif [ "${kbs}" != 0 ] ; then
        printf "%sK" "${kbs}"
    fi
fi
