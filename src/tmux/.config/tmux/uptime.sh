#!/bin/sh

# This file is part of mydot.
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later

totalseconds="$(awk -F . '{ print $1 }' < /proc/uptime)"

seconds=$((totalseconds % 60))
minutes=$(((totalseconds / 60) % 60))
hours=$(((totalseconds / 3600) % 24))
days=$((totalseconds / 86400))

if [ ${days} != 0 ] ; then
    printf "%sd " "${days}"
fi

if [ ${hours} != 0 ] ; then
    printf "%sh " "${hours}"
fi

if [ ${minutes} != 0 ] ; then
    printf "%sm" "${minutes}"
else
    printf "%ss" "${seconds}"
fi
