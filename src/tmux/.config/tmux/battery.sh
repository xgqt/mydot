#!/bin/sh

# This file is part of mydot.
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later

batteries="/sys/class/power_supply"

if [ -d "${batteries}" ] ; then
    :
else
    printf " "
fi

for b in "${batteries}"/?* ; do
    battery="$(basename "${b}")"

    if [ -e "/sys/class/power_supply/${battery}/energy_full" ]; then
        energy_full="$(cat "/sys/class/power_supply/${battery}/energy_full")"
        energy_now="$(cat "/sys/class/power_supply/${battery}/energy_now")"

        percentage=$((energy_now * 100 / energy_full))

        if [ ${percentage} -gt 60 ] ; then
            printf "#[fg=#b2b2b2]"
        elif [ ${percentage} -lt 15 ] ; then
            printf "#[fg=#d70000]"
        else
            printf "#[fg=#875f00]"
        fi

        printf "${percentage}%% "
    fi
done
