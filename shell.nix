let pkgs = import <nixpkgs> { };
in pkgs.mkShell rec {
  name = "mydot";

  nativeBuildInputs = with pkgs.buildPackages; [
    bash
    gnumake
    ncurses
    stow
    zsh

    # Dev/Tests/CI
    commitizen
    python3
    shellcheck

    # Fundamental packages
    git
    glibcLocales
  ];

  shellHook = ''
    PS1="${name}> "
  '';
}
