:;exec emacs --no-site-file -batch -q -l "${0}" -f main # -*- Emacs-Lisp -*-


;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License

;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:


;; Reload the MyDot configuration



;;; Code:


(defvar mydot-root-dir
  (expand-file-name "../" (file-name-directory load-file-name)))

(defvar mydot-emacs-dir
  (expand-file-name "src/emacs/.config/emacs/" mydot-root-dir))

(defvar mydot-emacs-assets
  (expand-file-name "assets/" mydot-emacs-dir))

(defvar mydot-emacs-site-lisp
  (expand-file-name "site-lisp/" mydot-emacs-assets))

(defvar mydot-emacs-org-config
  (expand-file-name "config/" mydot-emacs-assets))

(defvar mydot-emacs-snippets
  (expand-file-name "snippets/" mydot-emacs-assets))

(defvar mydot-crital-variables
  '(default-directory
    mydot-root-dir
    mydot-emacs-dir
    mydot-emacs-assets
    mydot-emacs-site-lisp
    mydot-emacs-org-config
    mydot-emacs-snippets))

(mapc (lambda (v)
        (message "Variable (emacs-reload): %s is set to: %s" v (eval v)))
      mydot-crital-variables)

;; Add assets to path
(add-to-list 'load-path mydot-emacs-site-lisp)


;; Load

;; Load init
(setenv "EMACS_ORG" "0")
(setenv "EMACS_SERVER" "0")

(load (expand-file-name "init.el" mydot-emacs-dir))

;; Force-eval `minor-customize-variables'.
(when (fboundp 'minor-customize-variables)
  (minor-customize-variables))

;; Install required packages
(if (equal (getenv "EMACS_NET") "0")
    (display-warning 'emacs-reload
                     "EMACS_NET is set to 0, not refreshing packages"
                     :warning)
  (mydot-install-requirements))

;; Reload config
(cd mydot-emacs-dir)
(mydot-load-either (expand-file-name "environment.org" mydot-emacs-org-config))
(mydot-config-reload)

;; Compile snippets
(when (require 'yasnippet nil t)
  (display-warning 'emacs-reload "Compiling Yasnippet snippets" :warning)
  (yas-compile-directory mydot-emacs-snippets))



;;; emacs_reload.el ends here
