:;exec emacs --no-site-file -batch -q -l "${0}" -f main # -*- Emacs-Lisp -*-


;; This file is part of mydot - XGQT's configuration files.
;; Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License

;; mydot is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; mydot is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with mydot.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:


;; Create MyDot autoloads.



;;; Code:


(defvar mydot-root-dir
  (expand-file-name "../" (file-name-directory load-file-name)))

(defvar mydot-emacs-dir
  (expand-file-name "src/emacs/.config/emacs/" mydot-root-dir))

(defvar mydot-emacs-assets
  (expand-file-name "assets/" mydot-emacs-dir))

(defvar mydot-emacs-site-lisp
  (expand-file-name "site-lisp/" mydot-emacs-assets))

(defvar mydot-emacs-autoloads
  (expand-file-name "autoloads.el" mydot-emacs-site-lisp))


(setq make-backup-files nil)

(setq generated-autoload-file mydot-emacs-autoloads)

(cond
 ((fboundp 'make-directory-autoloads)
  (make-directory-autoloads mydot-emacs-site-lisp mydot-emacs-autoloads))
 (t
  (update-directory-autoloads mydot-emacs-site-lisp)))



;;; emacs_autoloads.el ends here
