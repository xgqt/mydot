#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

$ErrorActionPreference = "Stop"

$userEmacsDirectory = Join-Path -Path $env:APPDATA -ChildPath ".emacs.d"

Write-Output "User Emacs directory is ${userEmacsDirectory}"

$elispSourceDirectory = Join-Path -Path (Get-Location) -ChildPath "src/emacs/.config/emacs"

Write-Output "ELisp source directory is ${elispSourceDirectory}"

if (Test-Path -Path $userEmacsDirectory) {
    Write-Output "Path ${userEmacsDirectory} already exists."
} else {
    Write-Output "Path ${userEmacsDirectory} does not yet exist, creating it."

    New-Item -Force -Type Directory $userEmacsDirectory | Out-Null
}

Write-Output "Copying Emacs Lisp source files from ${elispSourceDirectory} to ${userEmacsDirectory}"

$elispSources = Get-ChildItem $elispSourceDirectory

foreach ($elispSource in $elispSources) {
    Copy-Item -Force -Recurse -Path $elispSource.FullName -Destination $userEmacsDirectory
}
