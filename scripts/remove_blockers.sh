#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Remove potential blockers that do
# not allow mydot to be stowed correctly

trap "exit 128" INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

echo "[INFO] Running from: ${script_path}"
echo "[INFO] Entering directory: ${script_root}"

cd "${script_root}" || exit 1

type git >/dev/null || exit 1

cd "$(git rev-parse --show-toplevel)" || exit 1

pretend=0

case "${1}" in
    -p | -pretend | --pretend )  pretend=1  ;;
    -e | -execute | --execute )  pretend=0  ;;
esac

cd ./src || exit 1

for dir in * ; do
    [ ! -d "${dir}" ] && continue

    for file in $(git ls-files --full-name "${dir}") ; do
        to_be_linked="${HOME}$(echo "${file}" | sed "s|src/${dir}||")"
        echo "${dir} - ${to_be_linked}"

        if [ -f "${to_be_linked}" ] ; then
            if [ ${pretend} -gt 0 ] ; then
                echo "...... Exists"
            else
                if rm "${to_be_linked}" ; then
                    echo "...... Removed"
                else
                    echo "...... ERROR: Failed to remove"
                fi
            fi
        fi
    done
done
