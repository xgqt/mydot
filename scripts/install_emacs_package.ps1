#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

param(
    [string] $PackageName,
    [string] $Emacs = "emacs",
    [switch] $NoRefresh = $false,
    [switch] $NoVerify = $false
)

$ErrorActionPreference = "Stop"

if (-not (Get-Command -ErrorAction SilentlyContinue -Name $Emacs)) {
    $emacsSearchPaths = @(
        "/bin/emacs"
        "/usr/bin/emacs"
        "/usr/local/bin/emacs"
    )

    foreach ($emacsSearchPath in $emacsSearchPaths) {
        if (Test-Path $emacsSearchPath) {
            $Emacs = $emacsSearchPath

            break
        }
    }

    $ntEmacs = "C:\Program Files\Emacs\"

    if (Test-Path $ntEmacs) {
        $emacsNtVersions = Get-ChildItem $ntEmacs

        foreach ($emacsNtVersion in $emacsNtVersions) {
            $emacsNtBin = Join-Path $emacsNtVersion.FullName "bin\emacs.exe"

            if (Test-Path $emacsNtBin) {
                $Emacs = $emacsNtBin

                break
            }
        }
    }
}

$elispBlock = "(progn
  (require 'package)
  (when (equal `"True`" `"$NoVerify`")
    (setq package-check-signature nil))
  (setq package-archives
    '((`"elpa`"   . `"https://tromey.com/elpa/`")
      (`"gnu`"    . `"https://elpa.gnu.org/packages/`")
      (`"melpa`"  . `"https://melpa.org/packages/`")))
  (package-initialize)
  (when (equal `"False`" `"$NoRefresh`")
    (package-refresh-contents))
  (package-install '$PackageName))"
$emacsFlags = @(
    "-batch"
    "-nw"
    "-q"
    "--eval"
    $elispBlock
)

Write-Output "$Emacs $emacsFlags"

& $Emacs @emacsFlags
