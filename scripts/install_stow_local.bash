#!/usr/bin/env -S bash

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

set -e
set -u
trap 'exit 128' INT

declare -r app_version="2.4.0"
declare -r app_archive="stow-${app_version}.tar.gz"
declare -r app_url="https://git.savannah.gnu.org/cgit/stow.git/snapshot/${app_archive}"

declare -r temp_dir="$(mktemp -d --suffix=.mydot-stow-local)"

cd "${temp_dir}"
wget "${app_url}" -O "${app_archive}"
tar xf "${app_archive}"

cd "stow-${app_version}"
autoreconf -f -i -v

if [[ ! -e ChangeLog ]] ; then
    echo "" > ChangeLog
fi

declare -a -r configure_opts=(
    --bindir="${HOME}/.local/bin"
    --datarootdir="${HOME}/.local/share"
    --libdir="${HOME}/.local/lib"
    --with-pmdir="${HOME}/.local/lib/perl5"
)
sh ./configure "${configure_opts[@]}"

make -j1
make -j1 install

rm -f -r "${temp_dir}"
