#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

$ErrorActionPreference = "Stop"

$winget = "winget.exe"
$wingetOptions = @(
    "--accept-package-agreements"
    "--accept-source-agreements"
    "--source"
    "winget"
    "--verbose"
)
$requiredPackages = @(
    "7zip.7zip"
    "BurntSushi.ripgrep.GNU"
    "GNU.Emacs"
    "Git.Git"
    "GnuPG.Gpg4win"
    "Microsoft.DotNet.SDK.8"              # Remember to check and bump version.
    "Microsoft.PowerShell"
    "Microsoft.WindowsTerminal"
    "Neovim.Neovim"
    "Python.Python.3.12"                  # Remember to check and bump version.
    "sharkdp.bat"
)

& $winget install @wingetOptions @args --exact @requiredPackages
