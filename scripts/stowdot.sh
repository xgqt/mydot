#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# shellcheck disable=2039

set -u
trap "exit 128" INT

# Add user's Python scripts to "PATH".
PATH="${PATH}:${HOME}/.local/bin"
export PATH

remove=0
stow_impl=""

exists() {
    command -v "${1}" >/dev/null 2>&1
}

msg() {
    if exists tput ; then
        echo "$(tput bold)${*}$(tput sgr0)"
    else
        echo "${*}"
    fi
}

usage() {
    cat <<EOF
Usage: stowdot [OPTION]
stowdot - stow dotfiles

Options:
    -h  show avalible options
    -i  install
    -r  remove

Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License
EOF
}

err_msg() {
    msg " ERROR: ${*}"

    exit 1
}

emacs_version_check() {
    emacs -Q \
          --batch \
          --eval='(if (< (string-to-number emacs-version) 27.1) (error ""))'
}

# Commandline options

if [ -n "${1+x}" ] ; then
    case "${1}" in
        -h | -help | --help | help )
            usage

            exit 0
            ;;

        -i | --install | -install | install | "" )
            remove=0

            msg "Choosing the installation option"
            ;;

        -r | --remove | -remove | remove )
            remove=1

            msg "Choosing the removal option"
            ;;

        * )
            usage

            err_msg "Wrong argument: ${1}"
            ;;
    esac
fi

# Needed checks

# check for stow
if exists stow ; then
    stow_impl="stow"
elif exists xstow ; then
    stow_impl="xstow"
elif exists pystow ; then
    stow_impl="pystow"
else
    echo "There is none of supported 'stow' implementations on the sytem!"
    echo "If you don't want to use 'ln' manually please install any of:"
    echo "- stow"
    echo "- xstow"
    echo "- pystow"

    err_msg "No stow program! Exiting"
fi

msg "Stow implementation: ${stow_impl}"

# Go to mydot source
mydot_source="$(dirname "$(readlink -f "${0}")")/../src/"

cd "${mydot_source}" \
    || err_msg "Can't cd into ${mydot_source}"

# check needed directories
req_dirs="
    ${HOME}/.cache/gdb
    ${HOME}/.config/emacs
    ${HOME}/.config/git
    ${HOME}/.local/share/julia
    ${HOME}/.local/share/python/ipython/profile_default
    ${HOME}/Documents
"
for dir in ${req_dirs} ; do
    if [ -e "${dir}" ] ; then
        echo "${dir} exists, good"
    else
        mkdir -p "${dir}"
        echo "${dir} created"
    fi
done

# Use ~/.emacs.d if version of Emacs < 27.1
if exists emacs && [ -e "${HOME}/.emacs.d" ] ; then
    mv "${HOME}/.emacs.d" "${HOME}/.emacs.d.bak"

    if emacs_version_check ; then
        :
    else
        ln -s "${HOME}/.config/emacs" "${HOME}/.emacs.d"
    fi
fi

# Perform desired Stow action

# Then, we iterate through everything there
for target in * ; do
    # Check if it is a directory
    if [ -d "${target}" ] ; then

        # Finally, do some work
        if [ ${remove} -gt 0 ] ; then
            msg "Removing ${target}"
            ${stow_impl} -v -t "${HOME}" -D "${target}"
        else
            msg "Installing ${target}"
            ${stow_impl} -v -t "${HOME}" "${target}"
        fi
    fi
done
