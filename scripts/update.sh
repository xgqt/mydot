#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
#
# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

set -e
trap "exit 128" INT

prog_name="$(basename "${0}")"
prog_desc="aggressively update mydot"
prog_args=""

MAKE="gmake"
build_docs="NO"
force_update="NO"
recompile_emacs="NO"

# Needed for grep on git below.
LANG="C"
export LANG

usage() {
    cat <<EOF
Usage: ${prog_name} [OPTION]... ${prog_args}
${prog_name} - ${prog_desc}

Options:
    -D, --docs          build docs
    -E, --emacs         recompile Emacs configuration
    -V, --version       show program version
    -f, --force         force update
    -h, --help          show avalible options
    -m, --make <exe>    make implementation to use
EOF
}

version() {
    cat <<EOF
${prog_name} 9999

Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License
EOF
}

while [ -n "${1}" ] ; do
    case "${1}" in
        -D | --docs )
            build_docs=YES
            ;;
        -E | --emacs )
            recompile_emacs=YES
            ;;
        -V | -version | --version )
            version
            exit 0
            ;;
        -f | --force )
            force_update=YES
            ;;
        -m | --make )
            MAKE="${2}"
            shift
            ;;
        -h | -help | --help )
            usage
            exit 0
            ;;
        -* )
            version
            echo
            usage
            exit 1
            ;;
    esac
    shift
done

# First check if we have net
# so ping the main mydot hosting
if ! curl gitlab.com >/dev/null 2>&1 ; then
    echo " ERROR: Seems like we can not connect to the Internet!"
    exit 1
fi

# Go to mydot root
cd "$(dirname "$(readlink -f "${0}")")"/../

# Check if we have git installed
if ! type git >/dev/null 2>&1 ; then
    echo " ERROR: No git command found on the system!"
    exit 1
fi

# Check if there are updates
if [ ${force_update} = NO ] ; then
    if git remote show origin 2>/dev/null               \
            | grep "master"                             \
            | grep "up to date" >/dev/null ; then
        echo " WARNING: Seems like you are already up-to-date!"
        exit 0
    fi
fi

if ! type "${MAKE}" >/dev/null 2>&1 ; then
    echo " WARNING: \"Make\" implementation \"${MAKE}\" not found!"
    MAKE=$(command -v gmake 2>/dev/null || command -v make 2>/dev/null || echo "")
    echo " WARNING: Setting Make implementation to \"${MAKE}\"!"
fi

if [ -z "${MAKE}" ] ; then
    echo " WARNING: Falling back to the legacy update procedure!"
    sh ./scripts/stowdot.sh remove
    git reset --hard
    git pull
    sh ./scripts/stowdot.sh install
else
    "${MAKE}" update
    [ ${build_docs} = YES ] && "${MAKE}" docs
    [ ${recompile_emacs} = YES ] && "${MAKE}" emacs-reload
fi

echo " SUCCESS: mydot was successfully updated"
