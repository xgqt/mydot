PYSTOW_AR   := pystow-master.tar.gz
PYSTOW_URL  := https://gitlab.com/xgqt/pystow/-/archive/master/$(PYSTOW_AR)
PYSTOW_DIR  := $(PWD)/.cache/pystow_dir/

.cache/pystow_dir:
	$(MKDIR) -p $(PYSTOW_DIR)

.cache/pystow_dir/pystow-master.tar.gz: have-wget .cache/pystow_dir
	$(WGET) -P $(PYSTOW_DIR) $(PYSTOW_URL)

.cache/pystow_dir/pystow-master: .cache/pystow_dir/pystow-master.tar.gz
	$(TAR) fx $(PYSTOW_DIR)/$(PYSTOW_AR) -C $(PYSTOW_DIR)

.PHONY: pystow-install
pystow-install: have-python .cache/pystow_dir/pystow-master
	$(MAKE) -C $(PYSTOW_DIR)/pystow-master install
