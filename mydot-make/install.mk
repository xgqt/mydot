.PHONY: requirements
requirements: have-pip
	$(PIP) install --user -r $(EXTRAS)/python/requirements.txt

.PHONY: remove-blockers
remove-blockers:
	$(SH) $(SCRIPTS)/remove_blockers.sh

.PHONY: install
install: have-stow
	$(SH) $(SCRIPTS)/stowdot.sh
	@echo " Install finished succesfully"

# NOTICE: Keep dependencies order

.PHONY: reinstall
reinstall:
	$(MAKE) remove
	$(MAKE) install
	@echo " Reinstall finished succesfully"

package.json: have-python
	$(PYTHON) $(SCRIPTS)/render_package.json.py $(shell date '+%Y.%m.%d' | sed 's/\.0/\./g')

# This is a joke, do NOT use 'npm' to install MyDot, use 'gmake' instead!
.PHONY: npm-install
npm-install: have-npm have-stow package.json
	$(NPM) install -g

.PHONY: zsh-choose-greeter
zsh-choose-greeter: have-zsh
	$(ZSH) $(SRC)/zsh/.config/zsh/zsh-choose-greeter.sh

.PHONY: zsh-choose-theme
zsh-choose-theme: have-zsh
	$(ZSH) $(SRC)/zsh/.config/zsh/zsh-choose-theme.sh
