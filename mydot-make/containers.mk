CONTAINER_BUILDER       :=

CONTAINERS              := $(EXTRAS)/containers
CONTAINERFILE           := $(CONTAINERS)/Containerfile

.PHONY: container
container:
	$(CONTAINER_BUILDER) build --tag mydot --file $(CONTAINERFILE) $(PWD)

.PHONY: docker
docker: have-docker
	$(MAKE) CONTAINER_BUILDER=docker container

.PHONY: podman
podman: have-podman
	$(MAKE) CONTAINER_BUILDER=podman container
