.PHONY: clean-docs
clean-docs:
	if [ -d $(PWD)/docs ] ; then $(RMDIR) $(PWD)/docs ; fi

.PHONY: docs-man
docs-man: have-help2man have-pandoc
	$(SH) $(SCRIPTS)/manpages.sh

.PHONY: docs-org
docs-org: have-pandoc
	$(SH) $(SCRIPTS)/orgdocs.sh

docs: docs-man docs-org
	@echo " Documentation build finished succesfully"
