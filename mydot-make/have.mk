BASH        := bash
DOCKER      := docker
EMACS       := emacs
FIND        := find
GIT         := git
GPG         := gpg
GUILE       := guile
HELP2MAN    := help2man
IEX         := iex
MKDIR       := mkdir
NPM         := npm
PANDOC      := pandoc
PIP         := pip3
PODMAN      := podman
PYTHON      := python3
RACKET      := racket
RM          := rm -f -r
SBCL        := sbcl
SH          := sh
SHELLCHECK  := shellcheck
TAR         := tar
TYPE        := type
WGET        := wget
ZSH         := zsh

PYLINT      := $(PYTHON) -m pylint

.PHONY: have-bash
have-bash:
	@$(TYPE) $(BASH) >/dev/null

.PHONY: have-docker
have-docker:
	@$(TYPE) $(DOCKER) >/dev/null

.PHONY: have-emacs
have-emacs:
	@$(TYPE) $(EMACS) >/dev/null

.PHONY: have-git
have-git:
	@$(TYPE) $(GIT) >/dev/null

.PHONY: have-gpg
have-gpg:
	@$(TYPE) $(GPG) >/dev/null

.PHONY: have-guile
have-guile:
	@$(TYPE) $(GUILE) >/dev/null

.PHONY: have-help2man
have-help2man:
	@$(TYPE) $(HELP2MAN) >/dev/null

.PHONY: have-iex
have-iex:
	@$(TYPE) $(IEX) >/dev/null

.PHONY: have-npm
have-npm:
	@$(TYPE) $(NPM) >/dev/null

.PHONY: have-pandoc
have-pandoc:
	@$(TYPE) $(PANDOC) >/dev/null

.PHONY: have-pip
have-pip:
	@$(TYPE) $(PIP) >/dev/null

.PHONY: have-podman
have-podman:
	@$(TYPE) $(PODMAN) >/dev/null

.PHONY: have-python
have-python:
	@$(TYPE) $(PYTHON) >/dev/null

.PHONY: have-racket
have-racket:
	@$(TYPE) $(RACKET) >/dev/null

.PHONY: have-sbcl
have-sbcl:
	@$(TYPE) $(SBCL) >/dev/null

.PHONY: have-shellcheck
have-shellcheck:
	@$(TYPE) $(SHELLCHECK) >/dev/null

.PHONY: have-wget
have-wget:
	@$(TYPE) $(WGET) >/dev/null

.PHONY: have-zsh
have-zsh:
	@$(TYPE) $(ZSH) >/dev/null

.PHONY: have-stow
have-stow:
	@$(TYPE) stow 2>/dev/null || $(TYPE) xstow 2>/dev/null  \
	|| $(TYPE) pystow 2>/dev/null || $(MAKE) pystow-install

.PHONY: have-pylint
have-pylint: have-python
have-pylint:
	@$(PYTHON) -c "import pylint" >/dev/null

.PHONY: have
have:
	-$(MAKE) -s have-bash
	-$(MAKE) -s have-docker
	-$(MAKE) -s have-emacs
	-$(MAKE) -s have-git
	-$(MAKE) -s have-gpg
	-$(MAKE) -s have-guile
	-$(MAKE) -s have-help2man
	-$(MAKE) -s have-iex
	-$(MAKE) -s have-npm
	-$(MAKE) -s have-pandoc
	-$(MAKE) -s have-pip
	-$(MAKE) -s have-podman
	-$(MAKE) -s have-pylint
	-$(MAKE) -s have-python
	-$(MAKE) -s have-racket
	-$(MAKE) -s have-sbcl
	-$(MAKE) -s have-shellcheck
	-$(MAKE) -s have-stow
	-$(MAKE) -s have-wget
	-$(MAKE) -s have-zsh
