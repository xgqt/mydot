EMACS_ASSETS    := $(SRC)/emacs/.config/emacs/assets
SITE-LISP       := $(EMACS_ASSETS)/site-lisp
SNIPPETS        := $(EMACS_ASSETS)/snippets
AUTOLOADS       := $(SITE-LISP)/autoloads.el

EMACSFLAGS      := --batch --no-init-file --no-splash

.PHONY: clean-autoloads
clean-autoloads:
	if [ -f $(AUTOLOADS) ] ; then $(RM) $(AUTOLOADS) ; fi

.PHONY: clean-elc
clean-elc:
	$(FIND) $(EMACS_ASSETS) -depth -type f -name "*.elc" -exec $(RM) {} \;

.PHONY: clean-yas
clean-yas:
	$(FIND) $(SNIPPETS) -depth -type f -name ".yas*.el" -exec $(RM) {} \;

.PHONY: clean-emacs
clean-emacs: clean-elc clean-yas

.PHONY: emacs-elc
emacs-elc: have-emacs
	$(EMACS) $(EMACSFLAGS) \
		--eval "(byte-recompile-directory \"$(SITE-LISP)\" 0)"

$(AUTOLOADS):
	$(EMACS) $(EMACSFLAGS) --script $(SCRIPTS)/emacs_autoloads.el

.PHONY: emacs-autoloads
emacs-autoloads: have-emacs
	$(MAKE) clean-autoloads
	$(MAKE) $(AUTOLOADS)

.PHONY: emacs-reload
emacs-reload: have-emacs
	$(EMACS) $(EMACSFLAGS) --script $(SCRIPTS)/emacs_reload.el

.PHONY: emacs
emacs:
	$(MAKE) emacs-autoloads
	$(MAKE) emacs-reload
	$(MAKE) clean-emacs
	$(MAKE) emacs-elc

.PHONY: emacs-quick
emacs-quick:
	env EMACS_NET="0" EMACS_THEME="none" $(MAKE) emacs 2>&1
