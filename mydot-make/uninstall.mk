.PHONY: clean-cache
clean-cache:
	if [ -e $(CACHE) ] ; then $(RMDIR) $(CACHE) ; fi

.PHONY: clean-nodejs
clean-nodejs:
	$(FIND) $(PWD) -depth -type f -name 'package*.json' -exec $(RM) {} \;

.PHONY: clean-ar
clean-ar:
	$(FIND) $(PWD) -depth -type f -name '*.t*gz' -exec $(RM) {} \;
	$(FIND) $(PWD) -depth -type f -name '*.zip' -exec $(RM) {} \;

.PHONY: clean
clean:
	$(MAKE) clean-ar
	$(MAKE) clean-cache
	$(MAKE) clean-docs
	$(MAKE) clean-elc
	$(MAKE) clean-nodejs

.PHONY: remove
remove: have-stow
	$(SH) $(SCRIPTS)/stowdot.sh remove
	@echo " Uninstall finished succesfully"

# Alias
.PHONY: uninstall
uninstall: remove
