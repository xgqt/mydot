QUICKLISP_HOME  := ~/.local/share/quicklisp
QUICKLISP_URL   := https://beta.quicklisp.org

~/.local/share/quicklisp:
	$(MKDIR) -p $(QUICKLISP_HOME)

~/.local/share/quicklisp/quicklisp.lisp: have-wget ~/.local/share/quicklisp
	$(WGET) -P $(QUICKLISP_HOME) $(QUICKLISP_URL)/quicklisp.lisp

~/.local/share/quicklisp/quicklisp.lisp.asc: have-wget ~/.local/share/quicklisp
	$(WGET) -P $(QUICKLISP_HOME) $(QUICKLISP_URL)/quicklisp.lisp.asc

~/.local/share/quicklisp/release-key.txt: have-wget ~/.local/share/quicklisp
	$(WGET) -P $(QUICKLISP_HOME) $(QUICKLISP_URL)/release-key.txt

.PHONY: quicklisp-verify
quicklisp-verify: have-gpg ~/.local/share/quicklisp/quicklisp.lisp ~/.local/share/quicklisp/quicklisp.lisp.asc ~/.local/share/quicklisp/release-key.txt
	$(GPG) --import $(QUICKLISP_HOME)/release-key.txt
	$(GPG) --verify \
		$(QUICKLISP_HOME)/quicklisp.lisp.asc $(QUICKLISP_HOME)/quicklisp.lisp

.PHONY: quicklisp-install
quicklisp-install: have-sbcl quicklisp-verify
	$(SBCL) --load $(QUICKLISP_HOME)/quicklisp.lisp \
		--eval "(quicklisp-quickstart:install :path \"$(QUICKLISP_HOME)\")" \
		--eval "(quit)"
