# NOTICE: Does not exit with failure

.PHONY: test-elixir
test-elixir: have-iex
	$(IEX) --dot-iex $(SRC)/elixir/.iex.exs --eval ':c.q'

.PHONY: test-hyperlinks
test-hyperlinks: docs-org have-bash have-python
	$(BASH) $(TESTS)/test_hyperlinks.sh

.PHONY: test-pylint
test-pylint: have-python have-pylint
	$(PYTHON) $(TESTS)/test_pylint.py

.PHONY: test-emacs
test-emacs: emacs-reload

~/.cache/guile/ccache:
	$(MKDIR) -p ~/.cache/guile/ccache

.PHONY: test-guile
test-guile: have-guile ~/.cache/guile/ccache
	$(FIND) $(SRC)/guile -type f -exec $(GUILE) {} \;

.PHONY: test-racket
test-racket: have-racket
	$(RACKET) --load $(SRC)/racket/.config/racket/.racketrc --no-init-file

.PHONY: test-shellcheck
test-shellcheck: have-shellcheck
	$(PYTHON) $(TESTS)/test_shellcheck.py

.PHONY: test
test:
	$(MAKE) test-emacs
	$(MAKE) test-guile
	$(MAKE) test-racket
	$(MAKE) test-shellcheck
