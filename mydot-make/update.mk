.PHONY: git-reset
git-reset: have-git
	$(GIT) reset --hard

.PHONY: git-pull
git-pull: have-git
	$(GIT) pull

.PHONY: git-modules
git-modules: have-git
	$(GIT) submodule update --init

.PHONY: git-update
git-update:
	$(MAKE) git-reset
	$(MAKE) git-pull
	$(MAKE) git-modules

.PHONY: update
update: have-git
	$(MAKE) remove
	$(MAKE) git-update
	$(MAKE) install
	@echo " Update finished succesfully"
