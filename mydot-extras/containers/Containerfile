# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.


# - IMAGE:
#   Build the image.
#   $ cd ../..
#   $ podman build  --file mydot-extras/containers/Containerfile -t mydot .
#   $ podman image ls
# - CONTAINER:
#   Create a container.
#   $ podman container create -it --name mydot_latest mydot:latest
#   $ podman container start mydot_latest
# - ENTER:
#   Enter the container (in new shell, not the spawned one).
#   $ podman exec -ti mydot_latest bash
#   $ export TERM=xterm-256color
#   $ emacs -nw
# - KILL:
#   Attach and exit form the shell.
#   $ podman attach mydot_latest


FROM alpine:latest

LABEL maintainer "Maciej Barć <xgqt@xgqt.org>"

RUN mkdir -p /opt/mydot

COPY . /opt/mydot

ENV TERM=dumb

RUN apk add bash busybox git make ncurses stow zsh zsh-vcs
RUN cd /opt/mydot && make remove-blockers install

ENTRYPOINT ["/bin/bash", "-i", "-l"]
