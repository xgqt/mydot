#!/bin/sh

# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.

# Execute this with:
# curl "https://gitlab.com/xgqt/mydot/-/raw/master/mydot-extras/deployment/bootstrap.sh" | sh
# ... assuming "gitlab.com" is this repo's remote,
# ... you can also export CLONETO to clone mydot into a different location.

set -e
set -u
trap 'exit 128' INT

find_exe() {
    type "${1}" >/dev/null 2>&1
}

check_exe() {
    if find_exe "${1}" ; then
        echo " [OK ] Successfully found executable ${1}"
    else
        echo " [WRN] Failed to find executable ${1}"
        sleep 3
    fi
}

echo " [INF] Checking necessary programs..."
check_exe git
check_exe gmake
check_exe make
check_exe tput

echo " [INF] Checking optional programs..."
check_exe git-lfs
check_exe pystow
check_exe stow

echo " [INF] Checking auxiliary programs..."
check_exe bash
check_exe emacs
check_exe zsh

cloneto_default="${HOME}/source/public/gitlab.com/xgqt"
repo_url="https://gitlab.com/xgqt/mydot"

: "${CLONETO:=${cloneto_default}}"
mkdir -p "${CLONETO}"
cd "${CLONETO}"

echo " [INF] Cloning \"${repo_url}\" into directory: ${CLONETO}"
git clone --recursive --shallow-submodules --verbose "${repo_url}"
cd mydot

if find_exe gmake ; then
    echo " [DBG] Will use GNU Make to perform installation."
    make_exe="gmake"
else
    echo " [DBG] Will use standard Make to perform installation."
    make_exe="make"
fi

${make_exe} remove-blockers
${make_exe} install
