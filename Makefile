PWD         := $(shell pwd)

CACHE       := $(PWD)/.cache
EXTRAS      := $(PWD)/mydot-extras
SCRIPTS     := $(PWD)/scripts
SRC         := $(PWD)/src
TESTS       := $(PWD)/tests

.PHONY: all
all:
	@echo "Try: install, uninstall or test"

include mydot-make/have.mk

include mydot-make/install.mk
include mydot-make/uninstall.mk
include mydot-make/update.mk
include mydot-make/tests.mk

include mydot-make/containers.mk
include mydot-make/docs.mk
include mydot-make/emacs.mk
include mydot-make/pystow.mk
include mydot-make/quicklisp.mk
