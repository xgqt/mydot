#!/usr/bin/env python


"""
Test MyDot scripts using pylint.

pylint: https://pypi.org/project/pylint/
""" """
This file is part of mydot - XGQT's configuration files.
Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License

mydot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

mydot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mydot.  If not, see <https://www.gnu.org/licenses/>.
"""


import sys

from os import walk
from os.path import exists
from os.path import join

from subprocess import CalledProcessError
from subprocess import run


mypath = "./src"


def get_files(path: str) -> list:
    """List of all files in PATH."""

    files = []
    for (dirpath, _, filenames) in walk(path):
        for filename in filenames:
            filepath = join(dirpath, filename)
            if exists(filepath):                # for broken symbolic links
                files.append(filepath)
    return files


def get_python_files(path: str) -> list:
    """List of all Python files in PATH."""

    files = []

    for f in get_files(path):
        with open(f, "r", encoding="utf8") as opened_f:
            topline = opened_f.readline()

            if "#!" in topline and "python" in topline:
                files.append(f)

    return files


def pylint_files(path: str) -> list:
    """Execute 'pylint' on files in PATH."""

    exist_status = 0

    for f in get_python_files(path):
        print(f'>>> Checking file: "{f}"...')

        try:
            run(["pylint", f], check=True)

            print("    file is correct.")
        except CalledProcessError:
            exist_status = 1

            print("    there were errors found in the file.")

    return exist_status


def main():
    """The main function."""

    sys.exit(pylint_files(mypath))


if __name__ == '__main__':
    main()
