#!/usr/bin/env python


"""
Test MyDot scripts using shellcheck.
""" """
This file is part of mydot - XGQT's configuration files.
Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License

mydot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

mydot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mydot.  If not, see <https://www.gnu.org/licenses/>.
"""


import sys

from os import chdir
from os import path

from subprocess import CalledProcessError
from subprocess import run

from glob import glob


def main():
    """! Main."""

    script_path = path.realpath(__file__)
    script_root = path.dirname(script_path)
    source_root = path.realpath(path.join(script_root, ".."))

    chdir(source_root)

    shell_files = []

    shell_files.extend(glob("scripts/**/*.sh", recursive=True))
    shell_files.extend(glob("src/sh/.config/**/*.sh", recursive=True))
    shell_files.extend(glob("tests/**/*.sh", recursive=True))

    for shell_file in glob("src/scripts/.local/**/bin/*", recursive=True):
        with open(shell_file, "r", encoding="utf-8") as shell_file_opened:
            shebang_line = shell_file_opened.readline()

            if "sh" in shebang_line:
                shell_files.append(shell_file)

    shell_files.sort()

    shell_files_count_all = len(shell_files)
    shell_files_count_done = 0
    exist_status = 0

    for shell_file in shell_files:
        shell_files_count_done += 1
        check_progress = f"{shell_files_count_done}/{shell_files_count_all}"

        print(f"[{check_progress}] Checking file {shell_file} ...")

        try:
            run(["shellcheck", shell_file], check=True)

            print("    file is correct.")
        except CalledProcessError:
            exist_status = 1

            print("    there were errors found in the file.")

        print(f"    File {shell_file} done.")

    sys.exit(exist_status)


if __name__ == "__main__":
    main()
