#!/usr/bin/env bash


# This file is part of mydot - XGQT's configuration files.
# Copyright (c) 2019-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License

# mydot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# mydot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with mydot.  If not, see <https://www.gnu.org/licenses/>.


trap 'exit 128' INT


script_path="${0}"
script_root="$(dirname "${script_path}")"

echo "[INFO] Running from: ${script_path}"
echo "[INFO] Entering directory: ${script_root}"

cd "${script_root}" || exit 1


type git || exit 1

cd "$(git rev-parse --show-toplevel)" || exit 1


tempdir="$(mktemp -d)"
linksfile="${tempdir}/links.html"

mapfile -t files < <(find ./docs/html -name "*.html")


echo "[DEBUG]: ${tempdir}"
echo "[DEBUG]: ${linksfile}"


touch "${linksfile}"

for file in "${files[@]}" ; do
    if [[ -f "${file}" ]] ; then
        grep 'http.*://' "${file}" >> "${linksfile}"
    fi
done


linkchecker --ignore-url='^mailto:' --ignore-url='^file:'  \
            --check-extern "${linksfile}" || exit 1

rm -r "${tempdir}"
